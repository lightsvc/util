# lighsvc util components

## Description
This project contains utilities utilized by other lightsvc components, but are self-contained and reusable (and reused) enough to deserve its own component.
It is not the objective of this component to became a general purpose utilities components library to be used by any project. 
Its sole purpose is to serve the other lightsvc components and the features it contains are created by the other components demands.

## Usage

### Create a dependency on Maven

In your project pom.xml:

    <dependencies>
        
        <!-- Other project dependencies... -->
    
        <!-- Insert this dependency: -->
        <dependency>
            <groupId>lightsvc</groupId>
            <artifactId>util</artifactId>
            <version>---USE THE LAST VERSION HERE---</version>
        </dependency>
        
        <!-- Other project dependencies... -->
    </dependencies>

## Support

* Check the [docs](https://gitlab.com/lightsvc/util/-/tree/master/docs), especially the [How-To's](https://gitlab.com/lightsvc/util/-/tree/master/docs/how-tos.md "How-to's"). They provide solutions to the most common questions.
* Report bugs and open features request at [GitLab's integrated issue tracker](https://gitlab.com/lightsvc/util/-/issues) --- Please ask first in community chats or search for other similar issues already open.

## Contributing

All lightsvc components are open to contributions. Specifically this component features are based on the technical demands of all other components.

Check the support channels to contact the community.

## Authors and acknowledgment

- Tiago van den Berg

## License

Licensed under the GNU Lesser General Public License, Version 3 <COPYING.LESSER or https://www.gnu.org/licenses/lgpl-3.0.html> 

or 

Apache License, Version 2.0 <LICENSE.apache-2.0 or https://www.apache.org/licenses/LICENSE-2.0> 

depending of file. 

The license and copyright notice is specified on a header in each file.  