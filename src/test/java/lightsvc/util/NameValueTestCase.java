package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class NameValueTestCase {

  @Test
  public void compareTo_mustReturnCorrectOrdering_always() {
    var unorderedList = List.of(
      new NameValue("name2", "value1"),
      new NameValue("name2", "value1"),
      new NameValue("name2", null),
      new NameValue("name2", null),
      new NameValue("name1", "value2"),
      new NameValue("name1", "value2"),
      new NameValue("name1", "value1"),
      new NameValue("name1", "value1"),
      new NameValue("name1", null),
      new NameValue("name1", null),
      new NameValue(null, "value2"),
      new NameValue(null, "value2"),
      new NameValue(null, "value1"),
      new NameValue(null, "value1"),
      new NameValue(null, null)
    );
    var orderedList = List.of(
      new NameValue(null, null),
      new NameValue(null, "value1"),
      new NameValue(null, "value1"),
      new NameValue(null, "value2"),
      new NameValue(null, "value2"),
      new NameValue("name1", null),
      new NameValue("name1", null),
      new NameValue("name1", "value1"),
      new NameValue("name1", "value1"),
      new NameValue("name1", "value2"),
      new NameValue("name1", "value2"),
      new NameValue("name2", null),
      new NameValue("name2", null),
      new NameValue("name2", "value1"),
      new NameValue("name2", "value1")
    );
    var sortedList = new ArrayList<>(unorderedList);
    Collections.sort(sortedList);
    assertEquals(orderedList, sortedList);
  }

  @Test
  public void equals_mustReturnFalse_whenThisNameAndValueAreNotNullAndOtherNameAndValueAreNull() {
    var thiz = new NameValue("name", "value");
    var other = new NameValue(null, null);
    var returnedValue = thiz.equals(other);
    assertFalse(returnedValue);
  }

  @Test
  public void equals_mustReturnFalse_whenThisNameAndValueAreNullAndOtherNameAndValueAreNotNull() {
    var thiz = new NameValue(null, null);
    var other = new NameValue("name", "value");
    var returnedValue = thiz.equals(other);
    assertFalse(returnedValue);
  }

  @Test
  public void equals_mustReturnFalse_whenThisNameIsNotNullAndValueIsNullAndOtherNameIsNullAndValueIsNotNull() {
    var thiz = new NameValue("name", null);
    var other = new NameValue(null, "value");
    var returnedValue = thiz.equals(other);
    assertFalse(returnedValue);
  }

  @Test
  public void equals_mustReturnSameHashCode_whenNamesAndValuesAreEquals() {
    var nameValue1 = new NameValue("name", "value");
    var nameValue2 = new NameValue("name", "value");
    var returnedValue1 = nameValue1.hashCode();
    var returnedValue2 = nameValue2.hashCode();
    assertEquals(returnedValue1, returnedValue2);
  }

  @Test
  public void equals_mustReturnTrue_whenThisAndOtherNameAndValueAreNotNullAndEquals() {
    var thiz = new NameValue("name", "value");
    var other = new NameValue("name", "value");
    var returnedValue = thiz.equals(other);
    assertTrue(returnedValue);
  }

  @Test
  public void equals_mustReturnTrue_whenThisAndOtherNameAndValueAreNull() {
    var thiz = new NameValue(null, null);
    var other = new NameValue(null, null);
    var returnedValue = thiz.equals(other);
    assertTrue(returnedValue);
  }

  @Test
  public void hashCode_mustReturnDifferentValues_whenNamesAreDifferent() {
    var nameValue1 = new NameValue("name1", "value");
    var nameValue2 = new NameValue("name2", "value");
    var nameValue3 = new NameValue(null, "value");
    var returnedValue1 = nameValue1.hashCode();
    var returnedValue2 = nameValue2.hashCode();
    var returnedValue3 = nameValue3.hashCode();
    assertNotEquals(returnedValue1, returnedValue2);
    assertNotEquals(returnedValue1, returnedValue3);
    assertNotEquals(returnedValue2, returnedValue3);
  }

  @Test
  public void hashCode_mustReturnDifferentValues_whenValuesAreDifferent() {
    var nameValue1 = new NameValue("name", "value1");
    var nameValue2 = new NameValue("name", "value2");
    var nameValue3 = new NameValue("name", null);
    var returnedValue1 = nameValue1.hashCode();
    var returnedValue2 = nameValue2.hashCode();
    var returnedValue3 = nameValue3.hashCode();
    assertNotEquals(returnedValue1, returnedValue2);
    assertNotEquals(returnedValue1, returnedValue3);
    assertNotEquals(returnedValue2, returnedValue3);
  }

  @Test
  public void nameValues_mustReturnSameInstancesInArraysAsParameters_always() {
    var nameValue1 = new NameValue("name", "value");
    var nameValue2 = new NameValue("name", "value");
    var returnedValue = NameValue.nameValues(nameValue1, nameValue2);
    var expected = new NameValue[] {
      nameValue1, nameValue2
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void of_mustReturnNameValueWithSameNameAndValueParameters_always() {
    var name = "name";
    var value = "value";
    var returnedValue = NameValue.of(name, value);
    assertSame(name, returnedValue.name());
    assertSame(value, returnedValue.value());
  }

}
