package lightsvc.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class EscapeTestCase {

  @Test
  public void html_mustReturnEscapedString_whenValueContainsEscapableCharacters() {
    var value = """
      "&<> ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ
      ŒœŠšŸƒ
      ˆ˜
      ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ
      αβγδεζηθικλμνξοπρςστυφχψωϑϒϖ
         ‌‍‎‏
      –—‘’‚“”„†‡•…‰′″‹›‾⁄
      €
      ℑ℘ℜ™
      ℵ
      ←↑→↓↔↵
      ⇐⇑⇒⇓⇔
      ∀∂∃∅∇∈∉∋∏∑−∗√∝∞∠∧∨∩∪∫∴∼≅≈≠≡≤≥⊂⊃⊆⊇⊕⊗⊥⋅⌈⌉⌊⌋〈〉◊
      ♠♣♥♦
      ǵ
      """;
    var returnedValue = Escape.html(value);
    assertEquals(
      """
        &quot;&amp;&lt;&gt;&nbsp;&iexcl;&cent;&pound;&curren;&yen;&brvbar;&sect;&uml;&copy;&ordf;&laquo;&not;&shy;&reg;&macr;&deg;&plusmn;&sup2;&sup3;&acute;&micro;&para;&middot;&cedil;&sup1;&ordm;&raquo;&frac14;&frac12;&frac34;&iquest;&Agrave;&Aacute;&Acirc;&Atilde;&Auml;&Aring;&AElig;&Ccedil;&Egrave;&Eacute;&Ecirc;&Euml;&Igrave;&Iacute;&Icirc;&Iuml;&ETH;&Ntilde;&Ograve;&Oacute;&Ocirc;&Otilde;&Ouml;&times;&Oslash;&Ugrave;&Uacute;&Ucirc;&Uuml;&Yacute;&THORN;&szlig;&agrave;&aacute;&acirc;&atilde;&auml;&aring;&aelig;&ccedil;&egrave;&eacute;&ecirc;&euml;&igrave;&iacute;&icirc;&iuml;&eth;&ntilde;&ograve;&oacute;&ocirc;&otilde;&ouml;&divide;&oslash;&ugrave;&uacute;&ucirc;&uuml;&yacute;&thorn;&yuml;<br />
        &OElig;&oelig;&Scaron;&scaron;&Yuml;&fnof;<br />
        &circ;&tilde;<br />
        &Alpha;&Beta;&Gamma;&Delta;&Epsilon;&Zeta;&Eta;&Theta;&Iota;&Kappa;&Lambda;&Mu;&Nu;&Xi;&Omicron;&Pi;&Rho;&Sigma;&Tau;&Upsilon;&Phi;&Chi;&Psi;&Omega;<br />
        &alpha;&beta;&gamma;&delta;&epsilon;&zeta;&eta;&theta;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&rho;&sigmaf;&sigma;&tau;&upsilon;&phi;&chi;&psi;&omega;&thetasym;&upsih;&piv;<br />
        &ensp;&emsp;&thinsp;&zwnj;&zwj;&lrm;&rlm;<br />
        &ndash;&mdash;&lsquo;&rsquo;&sbquo;&ldquo;&rdquo;&bdquo;&dagger;&Dagger;&bull;&hellip;&permil;&prime;&Prime;&lsaquo;&rsaquo;&oline;&frasl;<br />
        &euro;<br />
        &image;&weierp;&real;&trade;<br />
        &alefsym;<br />
        &larr;&uarr;&rarr;&darr;&harr;&crarr;<br />
        &lArr;&uArr;&rArr;&dArr;&hArr;<br />
        &forall;&part;&exist;&empty;&nabla;&isin;&notin;&ni;&prod;&sum;&minus;&lowast;&radic;&prop;&infin;&ang;&and;&or;&cap;&cup;&int;&there4;&sim;&cong;&asymp;&ne;&equiv;&le;&ge;&sub;&sup;&sube;&supe;&oplus;&otimes;&perp;&sdot;&lceil;&rceil;&lfloor;&rfloor;&lang;&rang;&loz;<br />
        &spades;&clubs;&hearts;&diams;<br />
        &#501;<br />
        """,
      returnedValue
    );
  }

  @Test
  public void html_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Escape.html(value);
    assertNull(returnedValue);
  }

  @Test
  public void html_mustReturnValue_whenValueContainsOnlyRegularCharacters() {
    var value = "ABCDEFGHIJKLMNOPQRTLSTUVXYZabcdefghijklmnopqrstuvwxyz01234567890-=_+'!@#$%*()[]{}^,.;:?/";
    var returnedValue = Escape.html(value);
    assertEquals(
      value,
      returnedValue
    );
  }

  @Test
  public void javaScript_mustReturnEscapedString_whenValueContainsEscapableCharacters() {
    var value = "\b\n\t\f\r'/\"\\\u0000\u001f\u0080";
    var returnedValue = Escape.javaScript(value);
    assertEquals(
      "\\b\\n\\t\\f\\r\\'\\/\\\"\\\\\\u0000\\u001F\\u0080",
      returnedValue
    );
  }

  @Test
  public void javaScript_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Escape.javaScript(value);
    assertNull(returnedValue);
  }

  @Test
  public void javaScript_mustReturnValue_whenValueContainsOnlyRegularCharacters() {
    var value = "ABCDEFGHIJKLMNOPQRTLSTUVXYZabcdefghijklmnopqrstuvwxyz01234567890-=_+!@#$%*()[]{}^,.;:?";
    var returnedValue = Escape.javaScript(value);
    assertEquals(
      value,
      returnedValue
    );
  }

  @Test
  public void json_mustReturnEscapedString_whenValueContainsEscapableCharacters() {
    var value = "\b\n\t\f\r\"\\\u0000\u001f\u0080";
    var returnedValue = Escape.json(value);
    assertEquals(
      "\\b\\n\\t\\f\\r\\\"\\\\\\u0000\\u001F\\u0080",
      returnedValue
    );
  }

  @Test
  public void json_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Escape.json(value);
    assertNull(returnedValue);
  }

  @Test
  public void json_mustReturnValue_whenValueContainsOnlyRegularCharacters() {
    var value = "ABCDEFGHIJKLMNOPQRTLSTUVXYZabcdefghijklmnopqrstuvwxyz01234567890-=_+!@#$%*()[]{}^,.;:?'/";
    var returnedValue = Escape.json(value);
    assertEquals(
      value,
      returnedValue
    );
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Escape.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void xml_mustReturnEscapedString_whenValueContainsEscapableCharacters() {
    var value = "\"&<>'\u0080";
    var returnedValue = Escape.xml(value);
    assertEquals(
      "&quot;&amp;&lt;&gt;&apos;&#128;",
      returnedValue
    );
  }

  @Test
  public void xml_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Escape.xml(value);
    assertNull(returnedValue);
  }

  @Test
  public void xml_mustReturnValue_whenValueContainsOnlyRegularCharacters() {
    var value = "ABCDEFGHIJKLMNOPQRTLSTUVXYZ\nabcdefghijklmnopqrstuvwxyz01234567890-=_+!@#$%*()[]{}^,.;:?/";
    var returnedValue = Escape.xml(value);
    assertEquals(
      value,
      returnedValue
    );
  }

}
