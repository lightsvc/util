package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.function.IntFunction;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class ArraysTestCase {

  @Test
  public void concat_mustReturnConcatenatedArrays_whenValuesAreNonEmptyArrays() {
    var value1 = Arrays.of("One", "Two", "Three");
    var value2 = Arrays.of("Four", "Five", "Six");
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = Arrays.of("One", "Two", "Three", "Four", "Five", "Six");
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnConcatenatedArrays_whenValuesContainsEmptyAndNonEmptyArrays() {
    var value1 = new String[] {};
    var value2 = Arrays.of("Four", "Five", "Six");
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = Arrays.of("Four", "Five", "Six");
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnConcatenatedArrays_whenValuesContainsNonEmptyAndEmptyArrays() {
    var value1 = Arrays.of("One", "Two", "Three");
    var value2 = new String[] {};
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = Arrays.of("One", "Two", "Three");
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnConcatenatedArrays_whenValuesContainsNonEmptyAndNullArrays() {
    var value1 = Arrays.of("One", "Two", "Three");
    var value2 = (String[]) null;
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = Arrays.of("One", "Two", "Three");
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnConcatenatedArrays_whenValuesContainsNullAndNonEmptyArrays() {
    var value1 = (String[]) null;
    var value2 = Arrays.of("Four", "Five", "Six");
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = Arrays.of("Four", "Five", "Six");
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = Arrays.concat(String[]::new);
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnEmptyArray_whenValuesAreNullArrays() {
    var value1 = (String[]) null;
    var value2 = (String[]) null;
    var returnedValue = Arrays.concat(String[]::new, value1, value2);
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustReturnEmptyArray_whenValuesAreNullVarArgs() {
    var values = (String[][]) null;
    var returnedValue = Arrays.concat(String[]::new, values);
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void concat_mustThrowsNullPointerException_whenArrayGeneratorIsNull() {
    var array1 = Arrays.of("One", "Two", "Three");
    var array2 = Arrays.of("Four", "Five", "Six");
    var arrayGenerator = (IntFunction<String[]>) null;
    var thrownException = assertThrows(NullPointerException.class, ()-> Arrays.<String>concat(arrayGenerator, array1, array2));
    assertEquals("arrayGenerator must not be null.", thrownException.getMessage());
  }

  @Test
  public void concat_mustThrowsNullPointerException_whenArrayGeneratorReturnNull() {
    var array1 = Arrays.of("One", "Two", "Three");
    var array2 = Arrays.of("Four", "Five", "Six");
    var arrayGenerator = (IntFunction<String[]>) len->(String[]) null;
    var thrownException =  assertThrows(NullPointerException.class, ()-> Arrays.concat(arrayGenerator, array1, array2));
    assertEquals("arrayGenerator.apply() returned value must not be null.", thrownException.getMessage());
  }

  @Test
  public void concat_mustThrowsIllegalArgumentException_whenArrayGeneratorReturnsAnArraySmallerThanConcatenatedArraysItemsLength() {
    var array1 = Arrays.of("One", "Two", "Three");
    var array2 = Arrays.of("Four", "Five", "Six");
    var arrayGenerator = (IntFunction<String[]>) len->new String[len -1];
    var thrownException =  assertThrows(IllegalArgumentException.class, ()-> Arrays.concat(arrayGenerator, array1, array2));
    assertEquals("arrayGenerator.apply() returned value must return an array with length greater than the sum of arrays sizes.", thrownException.getMessage());
  }

  @Test
  public void isNotEmpty_mustReturnFalse_whenValueIsEmpty() {
    var value = new String[] {};
    var returnedValue = Arrays.isNotEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNotEmpty_mustReturnFalse_whenValueIsNull() {
    var value = (String[]) null;
    var returnedValue = Arrays.isNotEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNotEmpty_mustReturnTrue_whenValueIsNotEmptyMultiple() {
    var value = new String[] {
      "test", "test"
    };
    var returnedValue = Arrays.isNotEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void isNotEmpty_mustReturnTrue_whenValueIsNotEmptyOne() {
    var value = new String[] {
      "test"
    };
    var returnedValue = Arrays.isNotEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnFalse_whenValueIsNotEmptyMultiple() {
    var value = new String[] {
      "test", "test"
    };
    var returnedValue = Arrays.isNullOrEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnFalse_whenValueIsNotEmptyOne() {
    var value = new String[] {
      "test"
    };
    var returnedValue = Arrays.isNullOrEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnTrue_whenValueIsEmpty() {
    var value = new String[] {};
    var returnedValue = Arrays.isNullOrEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnTrue_whenValueIsNull() {
    var value = (String[]) null;
    var returnedValue = Arrays.isNullOrEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Arrays.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void of_mustReturnArrayWithValues_whenValuesAreVarArgs() {
    var returnedValue = Arrays.of("One", "Two", "Three");
    var expected = new String[] {
      "One", "Two", "Three"
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void of_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = Arrays.of();
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void of_mustReturnNull_whenValuesIsNull() {
    var values = (String[]) null;
    var returnedValue = Arrays.of(values);
    assertNull(returnedValue);
  }

  @Test
  public void of_mustReturnValues_whenValuesIsEmptyArray() {
    var values = new String[] {};
    var returnedValue = Arrays.of(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void of_mustReturnValues_whenValuesIsNotEmptyArray() {
    var values = new String[] {
      "One"
    };
    var returnedValue = Arrays.of(values);
    assertSame(values, returnedValue);
  }

}
