package lightsvc.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class DefaultValueTestCase {

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = DefaultValue.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void of_mustReturnDefaultValue_whenValueIsNull() {
    var value = (String) null;
    var defaultValue = "Two";
    var returnedValue = DefaultValue.of(value, defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void of_mustReturnSameValue_whenValueIsNotNull() {
    var value = "One";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.of(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void of_withSupplier_mustReturnDefaultValue_whenValueIsNull() {
    var value = (String) null;
    var defaultValue = "Two";
    var returnedValue = DefaultValue.of(value, ()->defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void of_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = "One";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.of(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }
  
  @Test
  public void ofBoolean_mustReturnSameValue_whenValueIsFalse() {
    var value = Boolean.valueOf(false);
    var defaultValue = true;
    var returnedValue = DefaultValue.ofBoolean(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofBoolean_mustReturnSameValue_whenValueIsTrue() {
    var value = Boolean.valueOf(true);
    var defaultValue = false;
    var returnedValue = DefaultValue.ofBoolean(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofBoolean_withSupplier_mustReturnSameValue_whenValueIsFalse() {
    var value = Boolean.valueOf(false);
    var defaultValue = true;
    var returnedValue = DefaultValue.ofBoolean(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofBoolean_withSupplier_mustReturnSameValue_whenValueIsTrue() {
    var value = Boolean.valueOf(true);
    var defaultValue = false;
    var returnedValue = DefaultValue.ofBoolean(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }
  
  @Test
  public void ofBoolean_mustReturnWrappedFalseDefaultValue_whenValueIsNull() {
    var value = (Boolean) null;
    var defaultValue = false;
    var returnedValue = DefaultValue.ofBoolean(value, defaultValue);
    assertFalse(returnedValue.booleanValue());
  }

  @Test
  public void ofBoolean_mustReturnWrappedTrueDefaultValue_whenValueIsNull() {
    var value = (Boolean) null;
    var defaultValue = true;
    var returnedValue = DefaultValue.ofBoolean(value, defaultValue);
    assertTrue(returnedValue.booleanValue());
  }

  @Test
  public void ofBoolean_withSupplier_mustReturnWrappedFalseDefaultValue_whenValueIsNull() {
    var value = (Boolean) null;
    var defaultValue = false;
    var returnedValue = DefaultValue.ofBoolean(value, ()->defaultValue);
    assertFalse(returnedValue.booleanValue());
  }

  @Test
  public void ofBoolean_withSupplier_mustReturnWrappedTrueDefaultValue_whenValueIsNull() {
    var value = (Boolean) null;
    var defaultValue = true;
    var returnedValue = DefaultValue.ofBoolean(value, ()->defaultValue);
    assertTrue(returnedValue.booleanValue());
  }
  
  @Test
  public void ofByte_mustReturnSameValue_whenValueIsNotNull() {
    var value = Byte.valueOf((byte) 1);
    var defaultValue = (byte) 2;
    var returnedValue = DefaultValue.ofByte(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofByte_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Byte) null;
    var defaultValue = (byte) 2;
    var returnedValue = DefaultValue.ofByte(value, defaultValue);
    assertEquals(defaultValue, returnedValue.byteValue());
  }

  @Test
  public void ofByte_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Byte.valueOf((byte) 1);
    var defaultValue = (byte) 2;
    var returnedValue = DefaultValue.ofByte(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofByte_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Byte) null;
    var defaultValue = (byte) 2;
    var returnedValue = DefaultValue.ofByte(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.byteValue());
  }

  @Test
  public void ofShort_mustReturnSameValue_whenValueIsNotNull() {
    var value = Short.valueOf((short) 1);
    var defaultValue = (short) 2;
    var returnedValue = DefaultValue.ofShort(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofShort_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Short) null;
    var defaultValue = (short) 2;
    var returnedValue = DefaultValue.ofShort(value, defaultValue);
    assertEquals(defaultValue, returnedValue.shortValue());
  }

  @Test
  public void ofShort_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Short.valueOf((short) 1);
    var defaultValue = (short) 2;
    var returnedValue = DefaultValue.ofShort(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofShort_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Short) null;
    var defaultValue = (short) 2;
    var returnedValue = DefaultValue.ofShort(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.shortValue());
  }

  @Test
  public void ofCharacter_mustReturnSameValue_whenValueIsNotNull() {
    var value = Character.valueOf('A');
    var defaultValue = 'B';
    var returnedValue = DefaultValue.ofCharacter(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofCharacter_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Character) null;
    var defaultValue = 'B';
    var returnedValue = DefaultValue.ofCharacter(value, defaultValue);
    assertEquals(defaultValue, returnedValue.charValue());
  }
  
  @Test
  public void ofCharacter_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Character.valueOf('A');
    var defaultValue = 'B';
    var returnedValue = DefaultValue.ofCharacter(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofCharacter_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Character) null;
    var defaultValue = 'B';
    var returnedValue = DefaultValue.ofCharacter(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.charValue());
  }

  @Test
  public void ofInteger_mustReturnSameValue_whenValueIsNotNull() {
    var value = Integer.valueOf(1);
    var defaultValue = 2;
    var returnedValue = DefaultValue.ofInteger(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofInteger_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Integer) null;
    var defaultValue = 2;
    var returnedValue = DefaultValue.ofInteger(value, defaultValue);
    assertEquals(defaultValue, returnedValue.intValue());
  }

  @Test
  public void ofInteger_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Integer.valueOf(1);
    var defaultValue = 2;
    var returnedValue = DefaultValue.ofInteger(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofInteger_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Integer) null;
    var defaultValue = 2;
    var returnedValue = DefaultValue.ofInteger(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.intValue());
  }

  @Test
  public void ofLong_mustReturnSameValue_whenValueIsNotNull() {
    var value = Long.valueOf(1L);
    var defaultValue = 2L;
    var returnedValue = DefaultValue.ofLong(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofLong_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Long) null;
    var defaultValue = 2L;
    var returnedValue = DefaultValue.ofLong(value, defaultValue);
    assertEquals(defaultValue, returnedValue.longValue());
  }

  @Test
  public void ofLong_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Long.valueOf(1L);
    var defaultValue = 2L;
    var returnedValue = DefaultValue.ofLong(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofLong_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Long) null;
    var defaultValue = 2L;
    var returnedValue = DefaultValue.ofLong(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.longValue());
  }

  @Test
  public void ofFloat_mustReturnSameValue_whenValueIsNotNull() {
    var value = Float.valueOf(1f);
    var defaultValue = 2f;
    var returnedValue = DefaultValue.ofFloat(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofFloat_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Float) null;
    var defaultValue = 2f;
    var returnedValue = DefaultValue.ofFloat(value, defaultValue);
    assertEquals(defaultValue, returnedValue.floatValue(), 0f);
  }

  @Test
  public void ofFloat_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Float.valueOf(1f);
    var defaultValue = 2f;
    var returnedValue = DefaultValue.ofFloat(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofFloat_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Float) null;
    var defaultValue = 2f;
    var returnedValue = DefaultValue.ofFloat(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.floatValue(), 0f);
  }

  @Test
  public void ofDouble_mustReturnSameValue_whenValueIsNotNull() {
    var value = Double.valueOf(1d);
    var defaultValue = 2d;
    var returnedValue = DefaultValue.ofDouble(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofDouble_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Double) null;
    var defaultValue = 2d;
    var returnedValue = DefaultValue.ofDouble(value, defaultValue);
    assertEquals(defaultValue, returnedValue.doubleValue(), 0d);
  }

  @Test
  public void ofDouble_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = Double.valueOf(1d);
    var defaultValue = 2d;
    var returnedValue = DefaultValue.ofDouble(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofDouble_withSupplier_mustReturnWrappedDefaultValue_whenValueIsNull() {
    var value = (Double) null;
    var defaultValue = 2d;
    var returnedValue = DefaultValue.ofDouble(value, ()->defaultValue);
    assertEquals(defaultValue, returnedValue.doubleValue(), 0d);
  }

  @Test
  public void ofString_mustReturnDefaultValue_whenValueIsEmpty() {
    var value = "";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void ofString_mustReturnDefaultValue_whenValueIsNull() {
    var value = (String) null;
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void ofString_mustReturnSameValue_whenValueIsNotNull() {
    var value = "One";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void ofString_withSupplier_mustReturnDefaultValue_whenValueIsEmpty() {
    var value = "";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, ()->defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void ofString_withSupplier_mustReturnDefaultValue_whenValueIsNull() {
    var value = (String) null;
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, ()->defaultValue);
    assertSame(defaultValue, returnedValue);
  }

  @Test
  public void ofString_withSupplier_mustReturnSameValue_whenValueIsNotNull() {
    var value = "One";
    var defaultValue = "Two";
    var returnedValue = DefaultValue.ofString(value, ()->defaultValue);
    assertSame(value, returnedValue);
  }

  @Test
  public void setIfNotNull_mustCallSetter_whenValueIsNotNull() {
    var value = "One";
    var holder = new Holder<String>();
    DefaultValue.setIfNotNull(value, v->holder.value = v);
    assertSame(value, holder.value);
  }

  @Test
  public void setIfNotNull_mustNotCallSetter_whenValueIsNull() {
    var value = (String) null;
    var holder = new Holder<String>();
    DefaultValue.setIfNotNull(value, v->holder.value = v);
    assertNull(holder.value);
  }

  @Test
  public void setIfNotNull_mustNotThrowError_whenSetterIsNull() {
    var value = "One";
    var holder = new Holder<String>();
    DefaultValue.setIfNotNull(value, null);
    assertNull(holder.value);
  }

  public static class Holder<Value> {

    public Value value = null;

  }

}
