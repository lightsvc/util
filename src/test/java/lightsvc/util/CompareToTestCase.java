package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class CompareToTestCase {

  public static final ComparableMock THISVALUE_NULL = null;
  public static final ComparableMock THISVALUE_VALID = new ComparableMock(0, null);
  public static final ComparableMock OTHERVALUE_NULL = null;
  public static final ComparableMock OTHERVALUE_VALID_1 = new ComparableMock(0, null);
  public static final ComparableMock OTHERVALUE_VALID_2 = new ComparableMock(0, null);
  public static final Function<ComparableMock, Object[]> ATTRIBUTESEXTRACTOR_NULL = null;
  public static final Function<ComparableMock, Comparable<?>[]> ATTRIBUTESEXTRACTOR_VALID = o->CompareTo.attributes(Integer.valueOf(o.a), o.b);

  @Test
  public void attributes_mustReturnArrayWithValues_whenValuesAreVarArgs() {
    var returnedValue = CompareTo.attributes("One", "Two", "Three");
    var expected = new String[] {
      "One", "Two", "Three"
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void attributes_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = CompareTo.attributes();
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void attributes_mustReturnNull_whenValuesIsNull() {
    var values = (String[]) null;
    var returnedValue = CompareTo.attributes(values);
    assertNull(returnedValue);
  }

  @Test
  public void attributes_mustReturnValues_whenValuesIsEmptyArray() {
    var values = new String[] {};
    var returnedValue = CompareTo.attributes(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void attributes_mustReturnValues_whenValuesIsNotEmptyArray() {
    var values = new String[] {
      "One"
    };
    var returnedValue = CompareTo.attributes(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = CompareTo.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void of_mustReturnNegativeInteger_whenOtherValueHasBiggerValueThanThisValue() {
    var thisValue = new ComparableMock(0, "One");
    var otherValue = new ComparableMock(0, "Two");
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue < 0);
    thisValue = new ComparableMock(0, "One");
    otherValue = new ComparableMock(1, "One");
    returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue < 0);
  }

  @Test
  public void of_mustReturnPositiveInteger_whenOtherValueHasBiggerValueThanThisValue() {
    var thisValue = new ComparableMock(0, "Two");
    var otherValue = new ComparableMock(0, "One");
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue > 0);
    thisValue = new ComparableMock(1, "One");
    otherValue = new ComparableMock(0, "One");
    returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue > 0);
  }

  @Test
  public void of_mustReturnNegativeInteger_whenOtherValueHasNullValueAndThisValueHasNotNull() {
    var thisValue = new ComparableMock(0, null);
    var otherValue = new ComparableMock(0, "One");
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue < 0);
  }

  @Test
  public void of_mustReturnPositiveInteger_whenThisValueHasNullValueAndOtherValueHasNotNull() {
    var thisValue = new ComparableMock(0, "One");
    var otherValue = new ComparableMock(0, null);
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue > 0);
  }

  @Test
  public void of_mustReturnPositiveInteger_whenOtherValueIsNull() {
    var thisValue = new ComparableMock(0, null);
    var otherValue = (ComparableMock) null;
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue > 0);
  }

  @Test
  public void of_mustReturnZero_whenAllAttributesAreEqual() {
    var thisValue = new ComparableMock(0, "One");
    var otherValue = new ComparableMock(0, "One");
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue == 0);
  }

  @Test
  public void of_mustReturnZero_whenOtherValueIsSameThisValue() {
    var thisValue = new ComparableMock(0, null);
    var otherValue = thisValue;
    var returnedValue = thisValue.compareTo(otherValue);
    assertTrue(returnedValue == 0);
  }

  @Test
  public void of_mustThrowsNullPointerException_whenAttributesExtractorIsNull() {
    var thisValue = new ComparableMock(0, null);
    var otherValue = (ComparableMock) null;
    var attributesExtractor = (Function<ComparableMock, Comparable<?>[]>) null;
    assertThrows(NullPointerException.class, ()->CompareTo.of(thisValue, otherValue, attributesExtractor));
  }

  @Test
  public void of_mustThrowsNullPointerException_whenThisValueIsNull() {
    var thisValue = (ComparableMock) null;
    var otherValue = (ComparableMock) null;
    var attributesExtractor = (Function<ComparableMock, Comparable<?>[]>) null;
    assertThrows(NullPointerException.class, ()->CompareTo.of(thisValue, otherValue, attributesExtractor));
  }

  @Test
  public void of_mustThrowsNullPointerException_whenTypeIsNull() {
    var thisValue = (ComparableMock) null;
    var otherValue = (ComparableMock) null;
    var attributesExtractor = (Function<ComparableMock, Comparable<?>[]>) null;
    assertThrows(NullPointerException.class, ()->CompareTo.of(thisValue, otherValue, attributesExtractor));
  }

  @Test
  public void collections_sort_mustReturnOrderedList_whenValueIsUnorderedList() {
    var item00 = (ComparableMock) null;
    var item01 = (ComparableMock) null;
    var item02 = new ComparableMock(0, null);
    var item03 = new ComparableMock(0, "One");
    var item04 = new ComparableMock(0, "Two");
    var item05 = new ComparableMock(1, null);
    var item06 = new ComparableMock(1, "One");
    var item07 = new ComparableMock(1, "Two");
    var unorderedList = java.util.Arrays.asList(item00, item04, item05, item03, item06, item02, item07, item01);
    var orderedList = java.util.Arrays.asList(item00, item01, item02, item03, item04, item05, item06, item07);

    var list = new ArrayList<>(unorderedList);
    Collections.sort(list, Comparator.nullsFirst(Comparator.naturalOrder()));
    assertEquals(orderedList, list);
  }

  public static class ComparableMock implements Comparable<ComparableMock> {

    public int a;
    public String b;

    public ComparableMock(final int a, final String b) {
      this.a = a;
      this.b = b;
    }

    @Override
    public int compareTo(ComparableMock o) {
      return CompareTo.of(this, o, v->CompareTo.attributes(Integer.valueOf(v.a), v.b));
    }

  }

}
