package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class EqualsTestCase {

  public static final Class<EqualityMock> TYPE_NULL = null;
  public static final Class<EqualityMock> TYPE_VALID = EqualityMock.class;
  public static final EqualityMock THISVALUE_NULL = null;
  public static final EqualityMock THISVALUE_VALID = new EqualityMock(0, null);
  public static final EqualityMock OTHERVALUE_NULL = null;
  public static final EqualityMock OTHERVALUE_VALID_1 = new EqualityMock(0, null);
  public static final EqualityMock OTHERVALUE_VALID_2 = new EqualityMock(0, null);
  public static final Function<EqualityMock, Object[]> ATTRIBUTESEXTRACTOR_NULL = null;
  public static final Function<EqualityMock, Object[]> ATTRIBUTESEXTRACTOR_VALID = o->Equals.attributes(Integer.valueOf(o.a), o.b);

  @Test
  public void attributes_mustReturnArrayWithValues_whenValuesAreVarArgs() {
    var returnedValue = Equals.attributes("One", "Two", "Three");
    var expected = new String[] {
      "One", "Two", "Three"
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void attributes_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = Equals.attributes();
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void attributes_mustReturnNull_whenValuesIsNull() {
    var values = (String[]) null;
    var returnedValue = Equals.attributes((Object[]) values);
    assertNull(returnedValue);
  }

  @Test
  public void attributes_mustReturnValues_whenValuesIsEmptyArray() {
    var values = new String[] {};
    var returnedValue = Equals.attributes((Object[]) values);
    assertSame(values, returnedValue);
  }

  @Test
  public void attributes_mustReturnValues_whenValuesIsNotEmptyArray() {
    var values = new String[] {
      "One"
    };
    var returnedValue = Equals.attributes((Object[]) values);
    assertSame(values, returnedValue);
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Equals.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueHasDifferentNonPrimitiveValues() {
    var thisValue = new EqualityMock(0, "One");
    var otherValue = new EqualityMock(0, "Two");
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueHasDifferentPrimitiveAttributes() {
    var thisValue = new EqualityMock(0, "One");
    var otherValue = new EqualityMock(1, "One");
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueIsDifferentAndThereIsANullAttributteOnOtherValue() {
    var thisValue = new EqualityMock(0, "One");
    var otherValue = new EqualityMock(0, null);
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueIsDifferentAndThereIsANullAttributteOnThisValue() {
    var thisValue = new EqualityMock(0, null);
    var otherValue = new EqualityMock(0, "One");
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueIsNotSubclassOfType() {
    var thisValue = new EqualitySubclass1Mock(0, "One");
    var otherValue = Integer.valueOf(0);
    @SuppressWarnings("unlikely-arg-type")
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnFalse_whenOtherValueIsNull() {
    var thisValue = new EqualityMock(0, null);
    var otherValue = (EqualityMock) null;
    var returnedValue = thisValue.equals(otherValue);
    assertFalse(returnedValue);
  }

  @Test
  public void of_mustReturnTrue_whenAllAttributesAreEqual() {
    var thisValue = new EqualityMock(0, "One");
    var otherValue = new EqualityMock(0, "One");
    var returnedValue = thisValue.equals(otherValue);
    assertTrue(returnedValue);
  }

  @Test
  public void of_mustReturnTrue_whenOtherValueIsSameThisValue() {
    var thisValue = new EqualityMock(0, null);
    var otherValue = thisValue;
    var returnedValue = thisValue.equals(otherValue);
    assertTrue(returnedValue);
  }

  @Test
  public void of_mustReturnTrue_whenOtherValueIsSubclassAndAllAttributesAreEqual() {
    var thisValue = new EqualityMock(0, "One");
    var otherValue = new EqualitySubclass1Mock(0, "One");
    var returnedValue = thisValue.equals(otherValue);
    assertTrue(returnedValue);
  }

  @Test
  public void of_mustReturnTrue_whenOtherValueIsSuperclassAndAllAttributesAreEqual() {
    var thisValue = new EqualitySubclass1Mock(0, "One");
    var otherValue = new EqualityMock(0, "One");
    var returnedValue = thisValue.equals(otherValue);
    assertTrue(returnedValue);
  }

  @Test
  public void of_mustReturnTrue_whenThisValueAndOtherValueAreDifferentSubclassesAndAllAttributesAreEqual() {
    var thisValue = new EqualitySubclass1Mock(0, "One");
    var otherValue = new EqualitySubclass2Mock(0, "One");
    @SuppressWarnings("unlikely-arg-type")
    var returnedValue = thisValue.equals(otherValue);
    assertTrue(returnedValue);
  }

  @Test
  public void of_mustThrowsNullPointerException_whenAttributesExtractorIsNull() {
    var type = EqualityMock.class;
    var thisValue = new EqualityMock(0, null);
    var otherValue = (EqualityMock) null;
    var attributesExtractor = (Function<EqualityMock, Object[]>) null;
    assertThrows(NullPointerException.class, ()->Equals.of(type, thisValue, otherValue, attributesExtractor));
  }

  @Test
  public void of_mustThrowsNullPointerException_whenThisValueIsNull() {
    var type = EqualityMock.class;
    var thisValue = (EqualityMock) null;
    var otherValue = (EqualityMock) null;
    var attributesExtractor = (Function<EqualityMock, Object[]>) null;
    assertThrows(NullPointerException.class, ()->Equals.of(type, thisValue, otherValue, attributesExtractor));
  }

  @Test
  public void of_mustThrowsNullPointerException_whenTypeIsNull() {
    var type = (Class<EqualityMock>) null;
    var thisValue = (EqualityMock) null;
    var otherValue = (EqualityMock) null;
    var attributesExtractor = (Function<EqualityMock, Object[]>) null;
    assertThrows(NullPointerException.class, ()->Equals.of(type, thisValue, otherValue, attributesExtractor));
  }

  public static class EqualityMock {

    public int a;
    public String b;

    public EqualityMock(final int a, final String b) {
      this.a = a;
      this.b = b;
    }

    @Override
    public boolean equals(final Object obj) {
      return Equals.of(EqualityMock.class, this, obj, o->Equals.attributes(Integer.valueOf(o.a), o.b));
    }

    @Override
    public int hashCode() {
      return Objects.hash(Integer.valueOf(this.a), this.b);
    }

  }

  public static class EqualitySubclass1Mock extends EqualityMock {

    public EqualitySubclass1Mock(final int a, final String b) {
      super(a, b);
    }

  }

  public static class EqualitySubclass2Mock extends EqualityMock {

    public EqualitySubclass2Mock(final int a, final String b) {
      super(a, b);
    }

  }

}
