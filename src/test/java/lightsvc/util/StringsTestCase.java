package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.function.UnaryOperator;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class StringsTestCase {

  @Test
  public void containsIsoControlCodes_mustReturnFalse_whenValueNotContainsIsoControlCodes() throws Exception {
    var value = "Some string without a control code character in it.";
    assertFalse(Strings.containsIsoControlCodes(value));
  }

  @Test
  public void containsIsoControlCodes_mustReturnTrue_whenValueContainsIsoControlCodes() throws Exception {
    for (var unicodeControlCode = (char) 0; unicodeControlCode < 32; unicodeControlCode++) {
      var value = "Some string with a control code character (" + unicodeControlCode + ") in it.";
      var returnedValue = Strings.containsIsoControlCodes(value);
      assertTrue(returnedValue);
    }
  }

  @Test
  public void containsIsoControlCodes_mustReturnTrue_whenValueIsNull() throws Exception {
    var value = (String) null;
    assertTrue(Strings.containsIsoControlCodes(value));
  }

  @Test
  public void isNotEmpty_mustReturnFalse_whenValueIsEmpty() {
    var value = "";
    var returnedValue = Strings.isNotEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNotEmpty_mustReturnFalse_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Strings.isNotEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNotEmpty_mustReturnTrue_whenValueIsNotEmpty() {
    var value = "a";
    var returnedValue = Strings.isNotEmpty(value);
    assertTrue(returnedValue);
    value = "ab";
    returnedValue = Strings.isNotEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnFalse_whenValueIsNotEmpty() {
    var value = "a";
    var returnedValue = Strings.isNullOrEmpty(value);
    assertFalse(returnedValue);
    value = "ab";
    returnedValue = Strings.isNullOrEmpty(value);
    assertFalse(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnTrue_whenValueIsEmpty() {
    var value = "";
    var returnedValue = Strings.isNullOrEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void isNullOrEmpty_mustReturnTrue_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Strings.isNullOrEmpty(value);
    assertTrue(returnedValue);
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Strings.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void nullSafe_mustReturnAnEmptyString_whenValueIsNull() {
    var value = (String) null;
    var returnedValue = Strings.nullSafe(value);
    assertEquals("", returnedValue);
  }

  @Test
  public void nullSafe_mustReturnSameValue_whenValueIsNotNull() {
    var value = "";
    var returnedValue = Strings.nullSafe(value);
    assertSame(value, returnedValue);
    value = "a";
    returnedValue = Strings.nullSafe(value);
    assertSame(value, returnedValue);
    value = "ab";
    returnedValue = Strings.nullSafe(value);
    assertSame(value, returnedValue);
  }

  @Test
  public void removeChars_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var charToRemove = ' ';
    var returnedValue = Strings.removeChars(value, charToRemove);
    assertNull(returnedValue);
  }

  @Test
  public void removeChars_mustReturnValue_whenValueNotContainsCharToRemove() {
    var value = "127.0.0.1, 127.0.0.2, 127.0.0.3";
    var charToRemove = '*';
    var returnedValue = Strings.removeChars(value, charToRemove);
    assertEquals(value, returnedValue);
  }

  @Test
  public void removeChars_mustReturnValueWithoutCharToRemove_whenValueContainsCharToRemove() {
    var value = "127.0.0.1, 127.0.0.2, 127.0.0.3";
    var charToRemove = ' ';
    var returnedValue = Strings.removeChars(value, charToRemove);
    var valueWithoutCharToRemove = "127.0.0.1,127.0.0.2,127.0.0.3";
    assertEquals(valueWithoutCharToRemove, returnedValue);
  }

  @Test
  public void removeChars_mustReturnValueWithoutCharToRemove_whenValueContainsConsecutiveCharsToRemove() {
    var value = "127.0.0.1,   127.0.0.3";
    var charToRemove = ' ';
    var returnedValue = Strings.removeChars(value, charToRemove);
    var valueWithoutCharToRemove = "127.0.0.1,127.0.0.3";
    assertEquals(valueWithoutCharToRemove, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var markupTagStart = "${";
    var markupTagEnd = "}";
    var replaceMap = Map.ofEntries(Map.entry("var5", "..."));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    assertNull(returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValue_whenMarkupTagEndIsNull() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1}.";
    var markupTagStart = "${";
    var markupTagEnd = (String) null;
    var replaceMap = Map.ofEntries(Map.entry("var5", "..."));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    assertEquals(value, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValue_whenMarkupTagsAreNotFound() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1}.";
    var markupTagStart = "${";
    var markupTagEnd = "}";
    var replaceMap = Map.ofEntries(Map.entry("var5", "..."));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    assertEquals(value, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValue_whenMarkupTagStartIsNull() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1}.";
    var markupTagStart = (String) null;
    var markupTagEnd = "}";
    var replaceMap = Map.ofEntries(Map.entry("var5", "..."));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    assertEquals(value, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValue_whenReplacerIsNull() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1}.";
    var markupTagStart = "${";
    var markupTagEnd = "}";
    var replacer = (UnaryOperator<String>) null;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    assertEquals(value, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValueWithReplaceValueParts_whenMarkupTagsAreFound() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1}.";
    var markupTagStart = "${";
    var markupTagEnd = "}";
    var replaceMap = Map.ofEntries(Map.entry("var1", "ipsum"), Map.entry("var2", "dolor"), Map.entry("var3", "$consect"), Map.entry("var4", "etur$"));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    var expected = "Lorem ipsum dolor $ sit amet, $$consectetur$ adipiscing elit ${var12}. ipsum.";
    assertEquals(expected, returnedValue);
  }

  @Test
  public void replaceMarkup_mustReturnValueWithReplaceValueParts_whenMarkupTagsAreFoundAndLastMarkupIsIncomplete() {
    var value = "Lorem ${var1} ${var2} $ sit amet, $${var3}${var4} adipiscing elit ${var12}. ${var1.";
    var markupTagStart = "${";
    var markupTagEnd = "}";
    var replaceMap = Map.ofEntries(Map.entry("var1", "ipsum"), Map.entry("var2", "dolor"), Map.entry("var3", "$consect"), Map.entry("var4", "etur$"));
    var replacer = (UnaryOperator<String>) replaceMap::get;
    var returnedValue = Strings.replaceMarkup(value, markupTagStart, markupTagEnd, replacer);
    var expected = "Lorem ipsum dolor $ sit amet, $$consectetur$ adipiscing elit ${var12}. ${var1.";
    assertEquals(expected, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithEmptyStringOnFirstItem_whenHasALeadingDelimiter() throws Exception {
    var value = " Leading Delimiter";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "", "Leading", "Delimiter"
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithEmptyStringOnLastItem_whenHasATrailingDelimiter() throws Exception {
    var value = "Trailing Delimiter ";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "Trailing", "Delimiter", ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithEmptyStringOnLastItems_whenHasDoubleTrailingDelimiters() throws Exception {
    var value = "Double-Trailing Delimiters  ";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "Double-Trailing", "Delimiters", "", ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithEmptyStringOnMiddle_whenHasAMiddleDoubleDelimiter() throws Exception {
    var value = "Double  Delimiters";
    var returnedValue = Strings.split(value, ' ');
    assertEquals(3, returnedValue.length);
    assertArrayEquals(new String[] {
      "Double", "", "Delimiters"
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithSplittedSegments_whenHasDelimiters() throws Exception {
    var value = " First Second Third Fourth ";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "", "First", "Second", "Third", "Fourth", ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithThreeEmptyString_whenValueIsDoubleDelimiter() throws Exception {
    var value = "  ";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "", "", ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithTwoEmptyString_whenValueIsDelimiter() throws Exception {
    var value = " ";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "", ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnArrayWithTwoEmptyStringOnFirstItems_whenHasDoubleLeadingDelimiters() throws Exception {
    var value = "  Double-Leading Delimiters";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      "", "", "Double-Leading", "Delimiters"
    }, returnedValue);
  }

  @Test
  public void split_mustReturnEmptyStringArray_whenValueIsNull() throws Exception {
    var value = (String) null;
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {}, returnedValue);
  }

  @Test
  public void split_mustReturnSingletonArrayWithEmptyString_whenValueIsEmpty() throws Exception {
    var value = "";
    var returnedValue = Strings.split(value, ' ');
    assertArrayEquals(new String[] {
      ""
    }, returnedValue);
  }

  @Test
  public void split_mustReturnSingletonArrayWithValue_whenDelimiterIsNotFound() throws Exception {
    var value = "This is a String without a wanted delimiter.";
    var returnedValue = Strings.split(value, '@');
    assertArrayEquals(new String[] {
      value
    }, returnedValue);
  }

  @Test
  public void trim_mustReturnEmptyString_whenValueOnlyContainsCharToTrim() {
    var value = "     ";
    var charToTrim = ' ';
    var returnedValue = Strings.trim(value, charToTrim);
    var expected = "";
    assertEquals(expected, returnedValue);
  }

  @Test
  public void trim_mustReturnNull_whenValueIsNull() {
    var value = (String) null;
    var charToTrim = ' ';
    var returnedValue = Strings.trim(value, charToTrim);
    assertNull(returnedValue);
  }

  @Test
  public void trim_mustReturnValue_whenValueNotContainsCharToTrim() {
    var value = "127.0.0.1, 127.0.0.2, 127.0.0.3";
    var charToTrim = '*';
    var returnedValue = Strings.trim(value, charToTrim);
    assertEquals(value, returnedValue);
  }

  @Test
  public void trim_mustReturnValueWithoutLeadingAndTrailingCharToTrim_whenValueContainsCharToTrim() {
    var value = "   127.0.0.1, 127.0.0.2, 127.0.0.3    ";
    var charToTrim = ' ';
    var returnedValue = Strings.trim(value, charToTrim);
    var valueWithoutCharToRemove = "127.0.0.1, 127.0.0.2, 127.0.0.3";
    assertEquals(valueWithoutCharToRemove, returnedValue);
  }

}
