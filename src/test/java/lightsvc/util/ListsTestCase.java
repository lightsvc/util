package lightsvc.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.processing.Generated;

import org.junit.Assert;
import org.junit.Test;

import lightsvc.util.Lists.BinarySearchInterval;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class ListsTestCase {

  public List<String> sortedList;
  public ArrayList<String> reversedSortedList;

  public ListsTestCase() {
    this.sortedList = List.of(
      // MISSING A
      "B",
      "C",
      "C",
      "D",
      "D",
      "D",
      // MISSING E
      "F",
      "G",
      "G",
      "H",
      "H",
      "H"
    // MISSING I
    );
    var tempReversedSortedList = new ArrayList<>(this.sortedList);
    Collections.reverse(tempReversedSortedList);
    this.reversedSortedList = tempReversedSortedList;
  }

  @Test
  public void binarySearchEnd_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(0, returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(0, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "I";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(0, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterItemIndex_whenComparatorIsNullFirstAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(0, null);
    var key = (String) null;
    var comparator = Comparator.nullsFirst(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.lastIndexOf(null) + 1, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterItemIndex_whenComparatorIsNullLastAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(null);
    var key = (String) null;
    var comparator = Comparator.nullsLast(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.lastIndexOf(null) + 1, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterItemIndex_whenKeyExistsInHighestValueInList() {
    var list = this.sortedList;
    var key = "H";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(list.lastIndexOf("H") + 1, returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.lastIndexOf("H") + 1, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterItemIndex_whenKeyExistsInLowestValueInList() {
    var list = this.sortedList;
    var key = "B";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(list.lastIndexOf("B") + 1, returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.lastIndexOf("B") + 1, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterItemIndex_whenKeyExistsInMiddleValueInList() {
    var list = this.sortedList;
    var key = "D";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(list.lastIndexOf("D") + 1, returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.lastIndexOf("D") + 1, returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInList() {
    var list = this.sortedList;
    var key = "I";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(list.size(), returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInReversedSortedList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInList() {
    var list = this.sortedList;
    var key = "E";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchEnd(list, key);
    Assert.assertEquals(list.indexOf("F"), returnedValue);
    returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.indexOf("F"), returnedValue);
  }

  @Test
  public void binarySearchEnd_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "E";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchEnd(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue);
  }

  @Test
  public void binarySearchEnd_mustThrowNullPointerException_whenComparatorIsNull() {
    var list = this.sortedList;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchEnd(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "comparator must not be null.");
  }

  @Test
  public void binarySearchEnd_mustThrowNullPointerException_whenListIsNull() {
    var list = (List<String>) null;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchEnd(list, key));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
    exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchEnd(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
  }

  @Test
  public void binarySearchInterval_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(0, returnedValue.start());
    Assert.assertEquals(0, returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(0, returnedValue.start());
    Assert.assertEquals(0, returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "I";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(0, returnedValue.start());
    Assert.assertEquals(0, returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(0, returnedValue.start());
    Assert.assertEquals(0, returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnInitialItemIndex_whenComparatorIsNullFirstAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(0, null);
    var key = (String) null;
    var comparator = Comparator.nullsFirst(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf(null), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf(null) + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnInitialItemIndex_whenComparatorIsNullLastAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(null);
    var key = (String) null;
    var comparator = Comparator.nullsLast(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf(null), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf(null) + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnInitialItemIndex_whenKeyExistsInHighestValueInList() {
    var list = this.sortedList;
    var key = "H";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(list.indexOf("H"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("H") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("H"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("H") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnInitialItemIndex_whenKeyExistsInLowestValueInList() {
    var list = this.sortedList;
    var key = "B";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(list.indexOf("B"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("B") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("B"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("B") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnInitialItemIndex_whenKeyExistsInMiddleValueInList() {
    var list = this.sortedList;
    var key = "D";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(list.indexOf("D"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("D") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue.start());
    Assert.assertEquals(list.lastIndexOf("D") + 1, returnedValue.end());
    Assert.assertTrue(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInList() {
    var list = this.sortedList;
    var key = "I";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(list.size(), returnedValue.start());
    Assert.assertEquals(list.size(), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue.start());
    Assert.assertEquals(list.size(), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInReversedSortedList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue.start());
    Assert.assertEquals(list.size(), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue.start());
    Assert.assertEquals(list.size(), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInList() {
    var list = this.sortedList;
    var key = "E";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchInterval(list, key);
    Assert.assertEquals(list.indexOf("F"), returnedValue.start());
    Assert.assertEquals(list.indexOf("F"), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("F"), returnedValue.start());
    Assert.assertEquals(list.indexOf("F"), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "E";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue.start());
    Assert.assertEquals(list.indexOf("D"), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
    returnedValue = Lists.binarySearchInterval(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue.start());
    Assert.assertEquals(list.indexOf("D"), returnedValue.end());
    Assert.assertFalse(returnedValue.found());
  }

  @Test
  public void binarySearchInterval_mustThrowNullPointerException_whenComparatorIsNull() {
    var list = this.sortedList;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchInterval(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "comparator must not be null.");
  }

  @Test
  public void binarySearchInterval_mustThrowNullPointerException_whenListIsNull() {
    var list = (List<String>) null;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchInterval(list, key));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
    exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchInterval(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
  }

  @Test
  public void BinarySearchInterval_new_mustThrowIllegalArgumentException_whenEndIsLesserThanStart() {
    var start = 0;
    var end = -1;
    var exception = Assert.assertThrows(IllegalArgumentException.class, ()->new BinarySearchInterval(start, end));
    Assert.assertEquals(exception.getMessage(), "end must not be lesser than start.");
  }

  @Test
  public void BinarySearchInterval_new_mustThrowIllegalArgumentException_whenStartIsNegative() {
    var start = -1;
    var end = 0;
    var exception = Assert.assertThrows(IllegalArgumentException.class, ()->new BinarySearchInterval(start, end));
    Assert.assertEquals(exception.getMessage(), "start must not be negative.");
  }

  @Test
  public void BinarySearchInterval_new_mustReturnBinarySearchInterval_whenStartAndEndAreDifferent() {
    var start = 1;
    var end = 2;
    var returnedValue = new BinarySearchInterval(start, end);
    Assert.assertEquals(start, returnedValue.start());
    Assert.assertEquals(end, returnedValue.end());
  }

  @Test
  public void BinarySearchInterval_new_mustReturnBinarySearchInterval_whenStartAndEndAreDifferentNozZero() {
    var start = 1;
    var end = 2;
    var returnedValue = new BinarySearchInterval(start, end);
    Assert.assertEquals(start, returnedValue.start());
    Assert.assertEquals(end, returnedValue.end());
  }

  @Test
  public void BinarySearchInterval_new_mustReturnBinarySearchInterval_whenStartAndEndAreEqualsAndNonZero() {
    var start = 2;
    var end = 2;
    var returnedValue = new BinarySearchInterval(start, end);
    Assert.assertEquals(start, returnedValue.start());
    Assert.assertEquals(end, returnedValue.end());
  }

  @Test
  public void BinarySearchInterval_new_mustReturnBinarySearchInterval_whenStartAndEndAreEqualsAndZero() {
    var start = 0;
    var end = 0;
    var returnedValue = new BinarySearchInterval(start, end);
    Assert.assertEquals(start, returnedValue.start());
    Assert.assertEquals(end, returnedValue.end());
  }

  @Test
  public void binarySearchStart_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(0, returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(0, returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnFirstIndex_whenKeyIsNonexistentLowestValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "I";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(0, returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnInitialItemIndex_whenComparatorIsNullFirstAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(0, null);
    var key = (String) null;
    var comparator = Comparator.nullsFirst(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf(null), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnInitialItemIndex_whenComparatorIsNullLastAndKeyIsNull() {
    var list = new ArrayList<>(this.sortedList);
    list.add(null);
    var key = (String) null;
    var comparator = Comparator.nullsLast(Comparator.<String>naturalOrder());
    var returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf(null), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnInitialItemIndex_whenKeyExistsInHighestValueInList() {
    var list = this.sortedList;
    var key = "H";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(list.indexOf("H"), returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf("H"), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnInitialItemIndex_whenKeyExistsInLowestValueInList() {
    var list = this.sortedList;
    var key = "B";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(list.indexOf("B"), returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf("B"), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnInitialItemIndex_whenKeyExistsInMiddleValueInList() {
    var list = this.sortedList;
    var key = "D";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(list.indexOf("D"), returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInList() {
    var list = this.sortedList;
    var key = "I";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(list.size(), returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnNextAfterLastIndex_whenKeyIsNonexistentHighestValueInReversedSortedList() {
    var list = this.sortedList;
    var key = "A";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.size(), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInList() {
    var list = this.sortedList;
    var key = "E";
    var comparator = Comparator.<String>naturalOrder();
    var returnedValue = Lists.binarySearchStart(list, key);
    Assert.assertEquals(list.indexOf("F"), returnedValue);
    returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf("F"), returnedValue);
  }

  @Test
  public void binarySearchStart_mustReturnNextItemIndex_whenKeyIsNonexistentMiddleValueInReversedSortedList() {
    var list = this.reversedSortedList;
    var key = "E";
    var comparator = Comparator.<String>reverseOrder();
    var returnedValue = Lists.binarySearchStart(list, key, comparator);
    Assert.assertEquals(list.indexOf("D"), returnedValue);
  }

  @Test
  public void binarySearchStart_mustThrowNullPointerException_whenComparatorIsNull() {
    var list = this.sortedList;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchStart(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "comparator must not be null.");
  }

  @Test
  public void binarySearchStart_mustThrowNullPointerException_whenListIsNull() {
    var list = (List<String>) null;
    var key = (String) null;
    var comparator = (Comparator<String>) null;
    var exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchStart(list, key));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
    exception = Assert.assertThrows(NullPointerException.class, ()->Lists.binarySearchStart(list, key, comparator));
    Assert.assertEquals(exception.getMessage(), "list must not be null.");
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Lists.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = Assert.assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    Assert.assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

}
