package lightsvc.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;

import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.annotation.processing.Generated;

import org.junit.Test;

@SuppressWarnings({
  "javadoc", "static-method"
})
@Generated("junit")
public class MapsTestCase {

  @Test
  public void entry_mustReturnMapEntryWithKeyAndValue_whenKeyAndValueAreNotNull() throws Exception {
    var key = "key";
    var value = "value";
    var entry = Maps.entry(key, value);
    assertEquals(key, entry.getKey());
    assertEquals(value, entry.getValue());
  }

  @Test
  public void entry_mustReturnMapEntryWithKeyAndValue_whenKeyIsIntAndValueIsNotNull() throws Exception {
    var key = 0;
    var value = "value";
    var entry = Maps.entry(key, value);
    assertEquals(key, entry.getKey().intValue());
    assertEquals(value, entry.getValue());
  }

  @Test
  public void entry_mustReturnMapEntryWithKeyAndValue_whenKeyIsIntAndValueIsNull() throws Exception {
    var key = 0;
    var value = (String) null;
    var entry = Maps.entry(key, value);
    assertEquals(key, entry.getKey().intValue());
    assertEquals(value, entry.getValue());
  }

  @Test
  public void entry_mustReturnMapEntryWithKeyAndValue_whenKeyIsNull() throws Exception {
    var key = (String) null;
    var value = "value";
    var entry = Maps.entry(key, value);
    assertEquals(key, entry.getKey());
    assertEquals(value, entry.getValue());
  }

  @Test
  public void entry_mustReturnMapEntryWithKeyAndValue_whenValueIsNull() throws Exception {
    var key = "key";
    var value = (String) null;
    var entry = Maps.entry(key, value);
    assertEquals(key, entry.getKey());
    assertEquals(value, entry.getValue());
  }

  @Test
  public void keys_mustReturnArrayWithValues_whenValuesAreVarArgs() {
    var returnedValue = Maps.keys("One", "Two", "Three");
    var expected = new String[] {
      "One", "Two", "Three"
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void keys_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = Maps.keys();
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void keys_mustReturnNull_whenValuesIsNull() {
    var values = (String[]) null;
    var returnedValue = Maps.keys(values);
    assertNull(returnedValue);
  }

  @Test
  public void keys_mustReturnValues_whenValuesIsEmptyArray() {
    var values = new String[] {};
    var returnedValue = Maps.keys(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void keys_mustReturnValues_whenValuesIsNotEmptyArray() {
    var values = new String[] {
      "One"
    };
    var returnedValue = Maps.keys(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void merge_mustReturnMapWithMapsToAddEntries_whenMapsToAddIsNotNull() throws Exception {
    var map = new TreeMap<String, String>();
    var mapsToAdd1 = Map.of("key", "value");
    var returnedValue = Maps.merge(map, mapsToAdd1);
    assertEquals(Map.of("key", "value"), returnedValue);
  }

  @Test
  public void merge_mustReturnMapWithPreexistentEntries_whenMapsToAddIsNull() throws Exception {
    var map = new TreeMap<>(Map.of("pre-existent-entry-key", "pre-existent-entry-value"));
    var mapsToAdd1 = (Map<String, String>) null;
    var returnedValue = Maps.merge(map, mapsToAdd1);
    assertEquals(Map.of("pre-existent-entry-key", "pre-existent-entry-value"), returnedValue);
  }

  @Test
  public void merge_mustReturnMapWithPreexistentEntriesAddedToNewEntries_whenMapsToAddContainsANullMapAndAMapWithNewEntries() throws Exception {
    var map = new TreeMap<>(Map.of("pre-existent-entry-key", "pre-existent-entry-value"));
    var mapsToAdd1 = (Map<String, String>) null;
    var mapsToAdd2 = Map.of("new-entry-key", "new-entry-value");
    var returnedValue = Maps.merge(map, mapsToAdd1, mapsToAdd2);
    assertEquals(Map.ofEntries(Map.entry("pre-existent-entry-key", "pre-existent-entry-value"), Map.entry("new-entry-key", "new-entry-value")), returnedValue);
  }

  @Test
  public void merge_mustReturnMapWithPreexistentEntriesAddedToNewEntries_whenMapsToAddContainsNewEntries() throws Exception {
    var map = new TreeMap<>(Map.of("pre-existent-entry-key", "pre-existent-entry-value"));
    var mapsToAdd1 = Map.of("new-entry-key", "new-entry-value");
    var returnedValue = Maps.merge(map, mapsToAdd1);
    assertEquals(Map.ofEntries(Map.entry("pre-existent-entry-key", "pre-existent-entry-value"), Map.entry("new-entry-key", "new-entry-value")), returnedValue);
  }

  @Test
  public void merge_mustReturnMapWithPreexistentEntriesAddedToNewEntriesMergedPrioritizingLaterMapsInMapsToAdd_whenMapsToAddMergeWithConflictingKeys()
    throws Exception {
    var map = new TreeMap<>(Map.of("pre-existent-entry-key", "pre-existent-entry-value"));
    var mapsToAdd1 = Map.of("new-entry-key", "new-entry-value");
    var mapsToAdd2 = Map.of("new-entry-key", "new-entry-overwritten-value");
    var returnedValue = Maps.merge(map, mapsToAdd1, mapsToAdd2);
    assertEquals(
      Map.ofEntries(Map.entry("pre-existent-entry-key", "pre-existent-entry-value"), Map.entry("new-entry-key", "new-entry-overwritten-value")),
      returnedValue
    );
  }

  @Test
  public void merge_mustReturnNull_whenMapIsNull() throws Exception {
    var map = (TreeMap<String, String>) null;
    var mapsToAdd1 = Map.of("key", "value");
    var returnedValue = Maps.merge(map, mapsToAdd1);
    assertNull(returnedValue);
  }

  @Test
  public void merge_mustReturnSameMap_whenMapIsNotNull() throws Exception {
    var map = new TreeMap<String, String>();
    var mapsToAdd1 = Map.of("key", "value");
    var returnedValue = Maps.merge(map, mapsToAdd1);
    assertSame(map, returnedValue);
  }

  @Test
  public void new_mustThrowUnsupportedOperationException_whenCalled() throws NoSuchMethodException, SecurityException {
    var constructor = Maps.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    var ite = assertThrows(InvocationTargetException.class, ()->constructor.newInstance());
    assertSame(UnsupportedOperationException.class, ite.getCause().getClass());
  }

  @Test
  public void of_mustReturnMapWithEntries_whenEntriesAreMultiple() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map, Maps.entry("key1", "value1"), Maps.entry("key2", "value2"));
    assertEquals(Map.of("key1", "value1", "key2", "value2"), returnedValue);
  }

  @Test
  public void of_mustReturnMapWithEntries_whenEntriesAreNullKeyed() {
    var map = new TreeMap<String, String>(Comparator.nullsFirst(Comparator.naturalOrder()));
    var returnedValue = Maps.of(map, Maps.entry(null, "value1"), Maps.entry("key2", "value2"));
    assertEquals(returnedValue.size(), 2);
    assertEquals(returnedValue.get(null), "value1");
    assertEquals(returnedValue.get("key2"), "value2");
  }

  @Test
  public void of_mustReturnMapWithEntries_whenEntriesAreNullValued() {
    var map = new TreeMap<String, String>(Comparator.nullsFirst(Comparator.naturalOrder()));
    var returnedValue = Maps.of(map, Maps.entry("key1", null), Maps.entry("key2", "value2"));
    assertEquals(returnedValue.size(), 2);
    assertNull(returnedValue.get("key1"));
    assertEquals(returnedValue.get("key2"), "value2");
  }

  @Test
  public void of_mustReturnMapWithEntries_whenEntriesAreSingleton() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map, Maps.entry("key1", "value1"));
    assertEquals(Map.of("key1", "value1"), returnedValue);
  }

  @Test
  public void of_mustReturnMapWithEntry_whenUsingDirectNonNullKeyAndValue() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map, "key1", "value1");
    assertEquals(Map.of("key1", "value1"), returnedValue);
  }

  @Test
  public void of_mustReturnMapWithNullEntriesIgnored_whenEntriesHaveNullEntries() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map, null, Maps.entry("key1", "value1"), null, Maps.entry("key2", "value2"), null);
    assertEquals(Map.of("key1", "value1", "key2", "value2"), returnedValue);
  }

  @Test
  public void of_mustReturnNull_whenMapIsNull() {
    var map = (Map<String, String>) null;
    var returnedValue = Maps.of(map);
    assertNull(returnedValue);
    returnedValue = Maps.of(map, new AbstractMap.SimpleEntry<>("key", "value"));
    assertNull(returnedValue);
  }

  @Test
  public void of_mustReturnNull_whenMapIsNullAndUsingDirectNonNullKeyAndValue() {
    var map = (Map<String, String>) null;
    var returnedValue = Maps.of(map, "key1", "value1");
    assertNull(returnedValue);
  }

  @Test
  public void of_mustReturnSameMap_whenEntriesIsNull() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map);
    var entries = (Entry<String, String>[]) null;
    assertSame(map, returnedValue);
    returnedValue = Maps.of(map, entries);
    assertSame(map, returnedValue);
  }

  @Test
  public void of_mustReturnSameMap_whenMapIsNotNull() {
    var map = new TreeMap<String, String>();
    var returnedValue = Maps.of(map);
    assertSame(map, returnedValue);
    returnedValue = Maps.of(map, new AbstractMap.SimpleEntry<>("key", "value"));
    assertSame(map, returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnMapWithAddedEntries_WhenKeysAndValuesAreSameLength() throws Exception {
    var map = new TreeMap<String, String>();
    var keys = Arrays.of("key1", "key2");
    var values = Arrays.of("value1", "value2");
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertEquals(Map.of("key1", "value1", "key2", "value2"), returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnMapWithAddedKeys_WhenKeysHasFewerItemsThanValues() throws Exception {
    var map = new TreeMap<String, String>();
    var keys = Arrays.of("key1", "key2");
    var values = Arrays.of("value1");
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertEquals(Map.of("key1", "value1"), returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnMapWithAddedValues_WhenValuesHasFewerItemsThanKeys() throws Exception {
    var map = new TreeMap<String, String>();
    var keys = Arrays.of("key1");
    var values = Arrays.of("value1", "value2");
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertEquals(Map.of("key1", "value1"), returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnNull_WhenMapIsNull() throws Exception {
    var map = (TreeMap<String, String>) null;
    var keys = Arrays.of("key1", "key2");
    var values = Arrays.of("value1", "value2");
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertNull(returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnSameMap_WhenKeysIsNull() throws Exception {
    var map = new TreeMap<String, String>();
    var keys = (String[]) null;
    var values = Arrays.of("value1", "value2");
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertSame(map, returnedValue);
  }

  @Test
  public void ofKeysAndValues_mustReturnSameMap_WhenValuesIsNull() throws Exception {
    var map = new TreeMap<String, String>();
    var keys = Arrays.of("key1", "key2");
    var values = (String[]) null;
    var returnedValue = Maps.ofKeysAndValues(map, keys, values);
    assertSame(map, returnedValue);
  }

  @Test
  public void values_mustReturnArrayWithValues_whenValuesAreVarArgs() {
    var returnedValue = Maps.values("One", "Two", "Three");
    var expected = new String[] {
      "One", "Two", "Three"
    };
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void values_mustReturnEmptyArray_whenValuesAreEmptyVarArgs() {
    var returnedValue = Maps.values();
    var expected = new String[] {};
    assertArrayEquals(expected, returnedValue);
  }

  @Test
  public void values_mustReturnNull_whenValuesIsNull() {
    var values = (String[]) null;
    var returnedValue = Maps.values(values);
    assertNull(returnedValue);
  }

  @Test
  public void values_mustReturnValues_whenValuesIsEmptyArray() {
    var values = new String[] {};
    var returnedValue = Maps.values(values);
    assertSame(values, returnedValue);
  }

  @Test
  public void values_mustReturnValues_whenValuesIsNotEmptyArray() {
    var values = new String[] {
      "One"
    };
    var returnedValue = Maps.values(values);
    assertSame(values, returnedValue);
  }

}
