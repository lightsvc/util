// Copyright 2022 Apache Software Foundation,
// Copyright 2022 Tiago van den Berg,
//
// This file is a Derivative Work as stated in Apache License, Version 2.0 from the Apache Commons Lang API Library.
// Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements.
//
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package lightsvc.util;

import java.util.HexFormat;
import java.util.Map;
import java.util.TreeMap;

/**
 * Escapes strings for Java, Java Script, HTML, and XML.
 *
 * @author Apache Software Foundation
 * @author Apache Jakarta Turbine
 * @author Purple Technology
 * @author Antony Riley
 * @author Helge Tesgaard
 * @author Phil Steitz
 * @author Pete Gieser
 * @author Tiago van den Berg
 */
public final class Escape {

  private static final HexFormat HEX = HexFormat.of().withUpperCase();
  private static final Map<Integer, String> BASIC_ENTITIES = Maps.of(
    new TreeMap<>(),
    Maps.entry(34, "quot"), // double-quote
    Maps.entry(38, "amp"), // ampersand
    Maps.entry(60, "lt"), // less-than
    Maps.entry(62, "gt") // greater-than
  );
  private static final Map<Integer, String> APOS_ENTITIES =  Maps.of(
    new TreeMap<>(),
    Maps.entry(39, "apos") // XML apostrophe
  );
  private static final Map<Integer, String> ISO8859_1_ENTITIES =  Maps.of(
    new TreeMap<>(),
    Maps.entry(160, "nbsp"), // non-breaking space
    Maps.entry(161, "iexcl"), // inverted exclamation mark
    Maps.entry(162, "cent"), // cent sign
    Maps.entry(163, "pound"), // pound sign
    Maps.entry(164, "curren"), // currency sign
    Maps.entry(165, "yen"), // yen sign = yuan sign
    Maps.entry(166, "brvbar"), // broken bar = broken vertical bar
    Maps.entry(167, "sect"), // section sign
    Maps.entry(168, "uml"), // diaeresis = spacing diaeresis
    Maps.entry(169, "copy"), // copyright sign
    Maps.entry(170, "ordf"), // feminine ordinal indicator
    Maps.entry(171, "laquo"), // left-pointing double angle quotation mark = left pointing guillemet
    Maps.entry(172, "not"), // not sign
    Maps.entry(173, "shy"), // soft hyphen = discretionary hyphen
    Maps.entry(174, "reg"), // registered trademark sign
    Maps.entry(175, "macr"), // macron = spacing macron = overline = APL overbar
    Maps.entry(176, "deg"), // degree sign
    Maps.entry(177, "plusmn"), // plus-minus sign = plus-or-minus sign
    Maps.entry(178, "sup2"), // superscript two = superscript digit two = squared
    Maps.entry(179, "sup3"), // superscript three = superscript digit three = cubed
    Maps.entry(180, "acute"), // acute accent = spacing acute
    Maps.entry(181, "micro"), // micro sign
    Maps.entry(182, "para"), // pilcrow sign = paragraph sign
    Maps.entry(183, "middot"), // middle dot = Georgian comma = Greek middle dot
    Maps.entry(184, "cedil"), // cedilla = spacing cedilla
    Maps.entry(185, "sup1"), // superscript one = superscript digit one
    Maps.entry(186, "ordm"), // masculine ordinal indicator
    Maps.entry(187, "raquo"), // right-pointing double angle quotation mark = right pointing guillemet
    Maps.entry(188, "frac14"), // vulgar fraction one quarter = fraction one quarter
    Maps.entry(189, "frac12"), // vulgar fraction one half = fraction one half
    Maps.entry(190, "frac34"), // vulgar fraction three quarters = fraction three quarters
    Maps.entry(191, "iquest"), // inverted question mark = turned question mark
    Maps.entry(192, "Agrave"), // uppercase A, grave accent
    Maps.entry(193, "Aacute"), // uppercase A, acute accent
    Maps.entry(194, "Acirc"), // uppercase A, circumflex accent
    Maps.entry(195, "Atilde"), // uppercase A, tilde
    Maps.entry(196, "Auml"), // uppercase A, umlaut
    Maps.entry(197, "Aring"), // uppercase A, ring
    Maps.entry(198, "AElig"), // uppercase AE
    Maps.entry(199, "Ccedil"), // uppercase C, cedilla
    Maps.entry(200, "Egrave"), // uppercase E, grave accent
    Maps.entry(201, "Eacute"), // uppercase E, acute accent
    Maps.entry(202, "Ecirc"), // uppercase E, circumflex accent
    Maps.entry(203, "Euml"), // uppercase E, umlaut
    Maps.entry(204, "Igrave"), // uppercase I, grave accent
    Maps.entry(205, "Iacute"), // uppercase I, acute accent
    Maps.entry(206, "Icirc"), // uppercase I, circumflex accent
    Maps.entry(207, "Iuml"), // uppercase I, umlaut
    Maps.entry(208, "ETH"), // uppercase Eth, Icelandic
    Maps.entry(209, "Ntilde"), // uppercase N, tilde
    Maps.entry(210, "Ograve"), // uppercase O, grave accent
    Maps.entry(211, "Oacute"), // uppercase O, acute accent
    Maps.entry(212, "Ocirc"), // uppercase O, circumflex accent
    Maps.entry(213, "Otilde"), // uppercase O, tilde
    Maps.entry(214, "Ouml"), // uppercase O, umlaut
    Maps.entry(215, "times"), // multiplication sign
    Maps.entry(216, "Oslash"), // uppercase O, slash
    Maps.entry(217, "Ugrave"), // uppercase U, grave accent
    Maps.entry(218, "Uacute"), // uppercase U, acute accent
    Maps.entry(219, "Ucirc"), // uppercase U, circumflex accent
    Maps.entry(220, "Uuml"), // uppercase U, umlaut
    Maps.entry(221, "Yacute"), // uppercase Y, acute accent
    Maps.entry(222, "THORN"), // uppercase THORN, Icelandic
    Maps.entry(223, "szlig"), // lowercase sharps, German
    Maps.entry(224, "agrave"), // lowercase a, grave accent
    Maps.entry(225, "aacute"), // lowercase a, acute accent
    Maps.entry(226, "acirc"), // lowercase a, circumflex accent
    Maps.entry(227, "atilde"), // lowercase a, tilde
    Maps.entry(228, "auml"), // lowercase a, umlaut
    Maps.entry(229, "aring"), // lowercase a, ring
    Maps.entry(230, "aelig"), // lowercase ae
    Maps.entry(231, "ccedil"), // lowercase c, cedilla
    Maps.entry(232, "egrave"), // lowercase e, grave accent
    Maps.entry(233, "eacute"), // lowercase e, acute accent
    Maps.entry(234, "ecirc"), // lowercase e, circumflex accent
    Maps.entry(235, "euml"), // lowercase e, umlaut
    Maps.entry(236, "igrave"), // lowercase i, grave accent
    Maps.entry(237, "iacute"), // lowercase i, acute accent
    Maps.entry(238, "icirc"), // lowercase i, circumflex accent
    Maps.entry(239, "iuml"), // lowercase i, umlaut
    Maps.entry(240, "eth"), // lowercase eth, Icelandic
    Maps.entry(241, "ntilde"), // lowercase n, tilde
    Maps.entry(242, "ograve"), // lowercase o, grave accent
    Maps.entry(243, "oacute"), // lowercase o, acute accent
    Maps.entry(244, "ocirc"), // lowercase o, circumflex accent
    Maps.entry(245, "otilde"), // lowercase o, tilde
    Maps.entry(246, "ouml"), // lowercase o, umlaut
    Maps.entry(247, "divide"), // division sign
    Maps.entry(248, "oslash"), // lowercase o, slash
    Maps.entry(249, "ugrave"), // lowercase u, grave accent
    Maps.entry(250, "uacute"), // lowercase u, acute accent
    Maps.entry(251, "ucirc"), // lowercase u, circumflex accent
    Maps.entry(252, "uuml"), // lowercase u, umlaut
    Maps.entry(253, "yacute"), // lowercase y, acute accent
    Maps.entry(254, "thorn"), // lowercase thorn, Icelandic
    Maps.entry(255, "yuml") // lowercase y, umlaut
  );
  // http://www.w3.org/TR/REC-html40/sgml/entities.html
  private static final Map<Integer, String> HTML40_ENTITIES =  Maps.of(
    new TreeMap<>(),
    Maps.entry(338, "OElig"), // -- latin capital ligature OE,U+0152 ISOlat2 -->
    Maps.entry(339, "oelig"), // -- latin small ligature oe, U+0153 ISOlat2 -->
    Maps.entry(352, "Scaron"), // -- latin capital letter S with caron,U+0160 ISOlat2 -->
    Maps.entry(353, "scaron"), // -- latin small letter s with caron,U+0161 ISOlat2 -->
    Maps.entry(376, "Yuml"), // -- latin capital letter Y with diaeresis,U+0178 ISOlat2 -->
    Maps.entry(402, "fnof"), // latin small f with hook = function= florin, U+0192 ISOtech -->
    Maps.entry(710, "circ"), // -- modifier letter circumflex accent,U+02C6 ISOpub -->
    Maps.entry(732, "tilde"), // small tilde, U+02DC ISOdia -->
    Maps.entry(913, "Alpha"), // greek capital letter alpha, U+0391 -->
    Maps.entry(914, "Beta"), // greek capital letter beta, U+0392 -->
    Maps.entry(915, "Gamma"), // greek capital letter gamma,U+0393 ISOgrk3 -->
    Maps.entry(916, "Delta"), // greek capital letter delta,U+0394 ISOgrk3 -->
    Maps.entry(917, "Epsilon"), // greek capital letter epsilon, U+0395 -->
    Maps.entry(918, "Zeta"), // greek capital letter zeta, U+0396 -->
    Maps.entry(919, "Eta"), // greek capital letter eta, U+0397 -->
    Maps.entry(920, "Theta"), // greek capital letter theta,U+0398 ISOgrk3 -->
    Maps.entry(921, "Iota"), // greek capital letter iota, U+0399 -->
    Maps.entry(922, "Kappa"), // greek capital letter kappa, U+039A -->
    Maps.entry(923, "Lambda"), // greek capital letter lambda,U+039B ISOgrk3 -->
    Maps.entry(924, "Mu"), // greek capital letter mu, U+039C -->
    Maps.entry(925, "Nu"), // greek capital letter nu, U+039D -->
    Maps.entry(926, "Xi"), // greek capital letter xi, U+039E ISOgrk3 -->
    Maps.entry(927, "Omicron"), // greek capital letter omicron, U+039F -->
    Maps.entry(928, "Pi"), // greek capital letter pi, U+03A0 ISOgrk3 -->
    Maps.entry(929, "Rho"), // greek capital letter rho, U+03A1 -->
    Maps.entry(931, "Sigma"), // greek capital letter sigma,U+03A3 ISOgrk3 -->
    Maps.entry(932, "Tau"), // greek capital letter tau, U+03A4 -->
    Maps.entry(933, "Upsilon"), // greek capital letter upsilon,U+03A5 ISOgrk3 -->
    Maps.entry(934, "Phi"), // greek capital letter phi,U+03A6 ISOgrk3 -->
    Maps.entry(935, "Chi"), // greek capital letter chi, U+03A7 -->
    Maps.entry(936, "Psi"), // greek capital letter psi,U+03A8 ISOgrk3 -->
    Maps.entry(937, "Omega"), // greek capital letter omega,U+03A9 ISOgrk3 -->
    Maps.entry(945, "alpha"), // greek small letter alpha,U+03B1 ISOgrk3 -->
    Maps.entry(946, "beta"), // greek small letter beta, U+03B2 ISOgrk3 -->
    Maps.entry(947, "gamma"), // greek small letter gamma,U+03B3 ISOgrk3 -->
    Maps.entry(948, "delta"), // greek small letter delta,U+03B4 ISOgrk3 -->
    Maps.entry(949, "epsilon"), // greek small letter epsilon,U+03B5 ISOgrk3 -->
    Maps.entry(950, "zeta"), // greek small letter zeta, U+03B6 ISOgrk3 -->
    Maps.entry(951, "eta"), // greek small letter eta, U+03B7 ISOgrk3 -->
    Maps.entry(952, "theta"), // greek small letter theta,U+03B8 ISOgrk3 -->
    Maps.entry(953, "iota"), // greek small letter iota, U+03B9 ISOgrk3 -->
    Maps.entry(954, "kappa"), // greek small letter kappa,U+03BA ISOgrk3 -->
    Maps.entry(955, "lambda"), // greek small letter lambda,U+03BB ISOgrk3 -->
    Maps.entry(956, "mu"), // greek small letter mu, U+03BC ISOgrk3 -->
    Maps.entry(957, "nu"), // greek small letter nu, U+03BD ISOgrk3 -->
    Maps.entry(958, "xi"), // greek small letter xi, U+03BE ISOgrk3 -->
    Maps.entry(959, "omicron"), // greek small letter omicron, U+03BF NEW -->
    Maps.entry(960, "pi"), // greek small letter pi, U+03C0 ISOgrk3 -->
    Maps.entry(961, "rho"), // greek small letter rho, U+03C1 ISOgrk3 -->
    Maps.entry(962, "sigmaf"), // greek small letter final sigma,U+03C2 ISOgrk3 -->
    Maps.entry(963, "sigma"), // greek small letter sigma,U+03C3 ISOgrk3 -->
    Maps.entry(964, "tau"), // greek small letter tau, U+03C4 ISOgrk3 -->
    Maps.entry(965, "upsilon"), // greek small letter upsilon,U+03C5 ISOgrk3 -->
    Maps.entry(966, "phi"), // greek small letter phi, U+03C6 ISOgrk3 -->
    Maps.entry(967, "chi"), // greek small letter chi, U+03C7 ISOgrk3 -->
    Maps.entry(968, "psi"), // greek small letter psi, U+03C8 ISOgrk3 -->
    Maps.entry(969, "omega"), // greek small letter omega,U+03C9 ISOgrk3 -->
    Maps.entry(977, "thetasym"), // greek small letter theta symbol,U+03D1 NEW -->
    Maps.entry(978, "upsih"), // greek upsilon with hook symbol,U+03D2 NEW -->
    Maps.entry(982, "piv"), // greek pi symbol, U+03D6 ISOgrk3 -->
    Maps.entry(8194, "ensp"), // en space, U+2002 ISOpub -->
    Maps.entry(8195, "emsp"), // em space, U+2003 ISOpub -->
    Maps.entry(8201, "thinsp"), // thin space, U+2009 ISOpub -->
    Maps.entry(8204, "zwnj"), // zero width non-joiner,U+200C NEW RFC 2070 -->
    Maps.entry(8205, "zwj"), // zero width joiner, U+200D NEW RFC 2070 -->
    Maps.entry(8206, "lrm"), // left-to-right mark, U+200E NEW RFC 2070 -->
    Maps.entry(8207, "rlm"), // right-to-left mark, U+200F NEW RFC 2070 -->
    Maps.entry(8211, "ndash"), // en dash, U+2013 ISOpub -->
    Maps.entry(8212, "mdash"), // em dash, U+2014 ISOpub -->
    Maps.entry(8216, "lsquo"), // left single quotation mark,U+2018 ISOnum -->
    Maps.entry(8217, "rsquo"), // right single quotation mark,U+2019 ISOnum -->
    Maps.entry(8218, "sbquo"), // single low-9 quotation mark, U+201A NEW -->
    Maps.entry(8220, "ldquo"), // left double quotation mark,U+201C ISOnum -->
    Maps.entry(8221, "rdquo"), // right double quotation mark,U+201D ISOnum -->
    Maps.entry(8222, "bdquo"), // double low-9 quotation mark, U+201E NEW -->
    Maps.entry(8224, "dagger"), // dagger, U+2020 ISOpub -->
    Maps.entry(8225, "Dagger"), // double dagger, U+2021 ISOpub -->
    Maps.entry(8240, "permil"), // per mille sign, U+2030 ISOtech -->
    Maps.entry(8249, "lsaquo"), // single left-pointing angle quotation mark,U+2039 ISO proposed -->
    Maps.entry(8250, "rsaquo"), // single right-pointing angle quotation mark,U+203A ISO proposed -->
    Maps.entry(8226, "bull"), // bullet = black small circle,U+2022 ISOpub -->
    Maps.entry(8230, "hellip"), // horizontal ellipsis = three dot leader,U+2026 ISOpub -->
    Maps.entry(8242, "prime"), // prime = minutes = feet, U+2032 ISOtech -->
    Maps.entry(8243, "Prime"), // double prime = seconds = inches,U+2033 ISOtech -->
    Maps.entry(8254, "oline"), // overline = spacing overscore,U+203E NEW -->
    Maps.entry(8260, "frasl"), // fraction slash, U+2044 NEW -->
    Maps.entry(8364, "euro"), // -- euro sign, U+20AC NEW -->
    Maps.entry(8465, "image"), // blackletter capital I = imaginary part,U+2111 ISOamso -->
    Maps.entry(8472, "weierp"), // script capital P = power set= Weierstrass p, U+2118 ISOamso -->
    Maps.entry(8476, "real"), // blackletter capital R = real part symbol,U+211C ISOamso -->
    Maps.entry(8482, "trade"), // trade mark sign, U+2122 ISOnum -->
    Maps.entry(8501, "alefsym"), // alef symbol = first transfinite cardinal,U+2135 NEW -->
    Maps.entry(8592, "larr"), // leftwards arrow, U+2190 ISOnum -->
    Maps.entry(8593, "uarr"), // upwards arrow, U+2191 ISOnum-->
    Maps.entry(8594, "rarr"), // rightwards arrow, U+2192 ISOnum -->
    Maps.entry(8595, "darr"), // downwards arrow, U+2193 ISOnum -->
    Maps.entry(8596, "harr"), // left right arrow, U+2194 ISOamsa -->
    Maps.entry(8629, "crarr"), // downwards arrow with corner leftwards= carriage return, U+21B5 NEW -->
    Maps.entry(8656, "lArr"), // leftwards double arrow, U+21D0 ISOtech -->
    Maps.entry(8657, "uArr"), // upwards double arrow, U+21D1 ISOamsa -->
    Maps.entry(8658, "rArr"), // rightwards double arrow,U+21D2 ISOtech -->
    Maps.entry(8659, "dArr"), // downwards double arrow, U+21D3 ISOamsa -->
    Maps.entry(8660, "hArr"), // left right double arrow,U+21D4 ISOamsa -->
    Maps.entry(8704, "forall"), // for all, U+2200 ISOtech -->
    Maps.entry(8706, "part"), // partial differential, U+2202 ISOtech -->
    Maps.entry(8707, "exist"), // there exists, U+2203 ISOtech -->
    Maps.entry(8709, "empty"), // empty set = null set = diameter,U+2205 ISOamso -->
    Maps.entry(8711, "nabla"), // nabla = backward difference,U+2207 ISOtech -->
    Maps.entry(8712, "isin"), // element of, U+2208 ISOtech -->
    Maps.entry(8713, "notin"), // not an element of, U+2209 ISOtech -->
    Maps.entry(8715, "ni"), // contains as member, U+220B ISOtech -->
    Maps.entry(8719, "prod"), // n-ary product = product sign,U+220F ISOamsb -->
    Maps.entry(8721, "sum"), // n-ary summation, U+2211 ISOamsb -->
    Maps.entry(8722, "minus"), // minus sign, U+2212 ISOtech -->
    Maps.entry(8727, "lowast"), // asterisk operator, U+2217 ISOtech -->
    Maps.entry(8730, "radic"), // square root = radical sign,U+221A ISOtech -->
    Maps.entry(8733, "prop"), // proportional to, U+221D ISOtech -->
    Maps.entry(8734, "infin"), // infinity, U+221E ISOtech -->
    Maps.entry(8736, "ang"), // angle, U+2220 ISOamso -->
    Maps.entry(8743, "and"), // logical and = wedge, U+2227 ISOtech -->
    Maps.entry(8744, "or"), // logical or = vee, U+2228 ISOtech -->
    Maps.entry(8745, "cap"), // intersection = cap, U+2229 ISOtech -->
    Maps.entry(8746, "cup"), // union = cup, U+222A ISOtech -->
    Maps.entry(8747, "int"), // integral, U+222B ISOtech -->
    Maps.entry(8756, "there4"), // therefore, U+2234 ISOtech -->
    Maps.entry(8764, "sim"), // tilde operator = varies with = similar to,U+223C ISOtech -->
    Maps.entry(8773, "cong"), // approximately equal to, U+2245 ISOtech -->
    Maps.entry(8776, "asymp"), // almost equal to = asymptotic to,U+2248 ISOamsr -->
    Maps.entry(8800, "ne"), // not equal to, U+2260 ISOtech -->
    Maps.entry(8801, "equiv"), // identical to, U+2261 ISOtech -->
    Maps.entry(8804, "le"), // less-than or equal to, U+2264 ISOtech -->
    Maps.entry(8805, "ge"), // greater-than or equal to,U+2265 ISOtech -->
    Maps.entry(8834, "sub"), // subset of, U+2282 ISOtech -->
    Maps.entry(8835, "sup"), // superset of, U+2283 ISOtech -->
    Maps.entry(8838, "sube"), // subset of or equal to, U+2286 ISOtech -->
    Maps.entry(8839, "supe"), // superset of or equal to,U+2287 ISOtech -->
    Maps.entry(8853, "oplus"), // circled plus = direct sum,U+2295 ISOamsb -->
    Maps.entry(8855, "otimes"), // circled times = vector product,U+2297 ISOamsb -->
    Maps.entry(8869, "perp"), // up tack = orthogonal to = perpendicular,U+22A5 ISOtech -->
    Maps.entry(8901, "sdot"), // dot operator, U+22C5 ISOamsb -->
    Maps.entry(8968, "lceil"), // left ceiling = apl upstile,U+2308 ISOamsc -->
    Maps.entry(8969, "rceil"), // right ceiling, U+2309 ISOamsc -->
    Maps.entry(8970, "lfloor"), // left floor = apl downstile,U+230A ISOamsc -->
    Maps.entry(8971, "rfloor"), // right floor, U+230B ISOamsc -->
    Maps.entry(9001, "lang"), // left-pointing angle bracket = bra,U+2329 ISOtech -->
    Maps.entry(9002, "rang"), // right-pointing angle bracket = ket,U+232A ISOtech -->
    Maps.entry(9674, "loz"), // lozenge, U+25CA ISOpub -->
    Maps.entry(9824, "spades"), // black spade suit, U+2660 ISOpub -->
    Maps.entry(9827, "clubs"), // black club suit = shamrock,U+2663 ISOpub -->
    Maps.entry(9829, "hearts"), // black heart suit = valentine,U+2665 ISOpub -->
    Maps.entry(9830, "diams") // black diamond suit, U+2666 ISOpub -->
  );
  private static final Map<Integer, String> XML_ENTITIES = Maps.merge(new TreeMap<>(), Escape.BASIC_ENTITIES, Escape.APOS_ENTITIES);
  private static final Map<Integer, String> HTML_ENTITIES = Maps.merge(
    new TreeMap<>(),
    Escape.BASIC_ENTITIES,
    Escape.ISO8859_1_ENTITIES,
    Escape.HTML40_ENTITIES
  );

  // Forbids the instantiation.
  private Escape() {
    throw new UnsupportedOperationException("lightsvc.util.Escape cannot be instantiated.");
  }

  /**
   * Escape text so it can be safely included in a HTML document as literal.
   *
   * @param value
   *   the text to be escape in HTML document.
   * 
   * @return the escaped text.
   */
  public static String html(final String value) {
    return Escape.xmlStyleString(value, Escape.HTML_ENTITIES, true);
  }

  /**
   * Escape text so it can be safely included in a javascript string as literal.
   *
   * @param value
   *   the text to be escape in javascript string.
   * 
   * @return the escaped text.
   */
  public static String javaScript(final String value) {
    return Escape.javaStyleString(value, true, true);
  }

  /**
   * Escape text so it can be safely included in a json string as literal.
   *
   * @param value
   *   the text to be escape in json string.
   * 
   * @return the escaped text.
   */
  public static String json(final String value) {
    return Escape.javaStyleString(value, false, false);
  }

  /**
   * Escape text so it can be safely included in a XML document as literal.
   *
   * @param value
   *   the text to be escape in XML document.
   * 
   * @return the escaped text.
   */
  public static String xml(final String value) {
    return Escape.xmlStyleString(value, Escape.XML_ENTITIES, false);
  }

  @SuppressWarnings("java:S1541") // SonarLint: Methods should not be too complex.
  private static String escapeUnicodesAndControlChars(final char ch, final boolean escapeSingleQuote, final boolean escapeForwardSlash) {
    if (ch > 0x7f) {
      return "\\u" + Escape.HEX.toHexDigits(ch);
    }
    switch (ch) {
      case '\b':
        return "\\b";
      case '\n':
        return "\\n";
      case '\t':
        return "\\t";
      case '\f':
        return "\\f";
      case '\r':
        return "\\r";
      case '\'':
        return escapeSingleQuote ? "\\'" : "'";
      case '"':
        return "\\\"";
      case '\\':
        return "\\\\";
      case '/':
        return escapeForwardSlash ? "\\/" : "/";
      default:
        break;
    }
    if (ch < 0x20) {
      return "\\u" + Escape.HEX.toHexDigits(ch);
    }
    return String.valueOf(ch);
  }

  private static String javaStyleString(final String value, final boolean escapeSingleQuote, final boolean escapeForwardSlash) {
    if (value == null) {
      return null;
    }
    var sb = new StringBuilder();
    var sz = value.length();
    for (var i = 0; i < sz; i++) {
      sb.append(Escape.escapeUnicodesAndControlChars(value.charAt(i), escapeSingleQuote, escapeForwardSlash));
    }
    return sb.toString();
  }

  private static String xmlStyleString(final String value, final Map<Integer, String> entities, final boolean escapeLineBreaks) {
    if (value == null) {
      return null;
    }
    var sb = new StringBuilder();
    var len = value.length();
    for (var i = 0; i < len; i++) {
      var c = value.charAt(i);
      var entityName = entities.get(Integer.valueOf(c));
      if (entityName != null) {
        sb.append('&');
        sb.append(entityName);
        sb.append(';');
        continue;
      }
      if ((c == '\n') && escapeLineBreaks) {
        sb.append("<br />\n");
        continue;
      }
      if (c > 0x7F) {
        sb.append("&#");
        sb.append(Integer.toString(c));
        sb.append(';');
        continue;
      }
      sb.append(c);
    }
    return sb.toString();
  }

}
