// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.Objects;
import java.util.function.IntFunction;

/**
 * Common utility methods, not present in Java SDK, to handle arrays.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class Arrays {

  // Forbids the instantiation.
  private Arrays() {
    throw new UnsupportedOperationException("lightsvc.util.Arrays cannot be instantiated.");
  }

  /**
   * Creates an array, copy the items of all arrays into it and returns it.
   *
   * @param <ArrayType>
   *   The type of the return array (automatically determined).
   * @param arrayGenerator
   *   The array constructor (Generally a lambda expression of a array constructor. For example: <code>String[]::new</code>.). <br />
   *   The sum of lengths of all {@code arrays} will be passed as argument.
   * @param arrays
   *   A varargs collection of arrays that will have their items copied in the new array. <br />
   *   Any {@code null} array will be ignored.
   * 
   * @return A newly created array with all concatenated items of {@code arrays}.
   * 
   * @throws NullPointerException
   *   if arrayGenerator is {@code null}, or it returns a {@code null}.
   * @throws IllegalArgumentException
   *   if arrayGenerator returns an array smaller than the sum of concatenated {@code arrays} items length.
   */
  @SafeVarargs
  public static <ArrayType> ArrayType[] concat(
    final IntFunction<ArrayType[]> arrayGenerator,
    final ArrayType[]... arrays
  ) throws NullPointerException, IllegalArgumentException {
    Objects.requireNonNull(arrayGenerator, "arrayGenerator must not be null.");
    // Calculate the size of final array...
    var len = 0;
    if (arrays != null) {
      for (var array : arrays) {
        if (array == null) {
          continue;
        }
        len += array.length;
      }
    }
    // ..And create it
    var concatenatedArray = arrayGenerator.apply(len);
    Objects.requireNonNull(concatenatedArray, "arrayGenerator.apply() returned value must not be null.");
    if (concatenatedArray.length < len) {
      throw new IllegalArgumentException("arrayGenerator.apply() returned value must return an array with length greater than the sum of arrays sizes.");
    }
    if (arrays == null) {
      return concatenatedArray;
    }
    var destPos = 0;
    for (var array : arrays) {
      if (array == null) {
        continue;
      }
      System.arraycopy(array, 0, concatenatedArray, destPos, array.length);
      destPos += array.length;
    }
    return concatenatedArray;
  }

  /**
   * Verifies if an array is not null and not empty.
   *
   * @param <Value>
   *   The type of {@code value} (automatically determined).
   * @param value
   *   the array to verify.
   * 
   * @return {@code true} if {@code value} is not null and not an empty array, {@code false} otherwise.
   */
  public static <Value> boolean isNotEmpty(final Value[] value) {
    return (value != null) && (value.length > 0);
  }

  /**
   * Verifies if an array is null or empty.
   *
   * @param <Value>
   *   The type of {@code value} (automatically determined).
   * @param value
   *   the array to verify.
   * 
   * @return {@code true} if {@code value} is null or an empty array, {@code false} otherwise.
   */
  public static <Value> boolean isNullOrEmpty(final Value[] value) {
    return (value == null) || (value.length == 0);
  }

  /**
   * Converts a collection of varargs objects into an array. It is just a syntactic sugar for an array constructor. For example:
   * <code>Arrays.of("One", "Two", "Three")</code> instead of <code> new String[] {"One", "Two", "Three"}</code>.
   *
   * @param <Value>
   *   The type of {@code values} (automatically determined).
   * @param values
   *   A varargs collection of objects.
   * 
   * @return The {@code values} array.
   */
  @SuppressWarnings("unchecked")
  public static <Value> Value[] of(final Value... values) {
    return values;
  }

}
