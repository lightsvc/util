// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Common utility methods, not present in Java SDK, to handle lists.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class Lists {

  // Forbids the instantiation.
  private Lists() {
    throw new UnsupportedOperationException("lightsvc.util.Lists cannot be instantiated.");
  }

  /**
   * Searches the specified naturally ordered {@code list} for the first (start inclusive) specified {@code key} using the binary search algorithm. If
   * {@code list} is not ordered, the result is undefined. This method differs from {@link Collections#binarySearch(List, Object)}, because it search the very
   * first element that match keys instead of any occurrence like Java SDK.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * 
   * @return the first index of the search key (start inclusive), if it is contained in the list; otherwise, the insertion index.
   * 
   * @throws NullPointerException
   *   when {@code list} is {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type extends Comparable<Type>> int binarySearchStart(final List<Type> list, final Type key) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    return Lists.binarySearch(list, key, Comparator.naturalOrder(), false);
  }

  /**
   * Searches the specified, ${code comparator} defined, ordered {@code list} for the first (start inclusive) specified {@code key} using the binary search
   * algorithm. If {@code list} is not ordered, the result is undefined. This method differs from {@link Collections#binarySearch(List, Object)}, because it
   * search the very first element that match keys instead of any occurrence like Java SDK.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * @param comparator
   *   the comparator by which the list is ordered.
   * 
   * @return the first index of the search key (start inclusive), if it is contained in the list; otherwise, the insertion index.
   * 
   * @throws NullPointerException
   *   when {@code list} or {@code comparator} are {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type> int binarySearchStart(final List<Type> list, final Type key, final Comparator<Type> comparator) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    Objects.requireNonNull(comparator, "comparator must not be null.");
    return Lists.binarySearch(list, key, comparator, false);
  }

  /**
   * Searches the specified naturally ordered {@code list} for the last (end exclusive) specified {@code key} using the binary search algorithm. If {@code list}
   * is not ordered, the result is undefined. This method differs from {@link Collections#binarySearch(List, Object)}, because it search the last element that
   * match keys instead of any occurrence like Java SDK.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * 
   * @return the index after the last found search key (end exclusive), if it is contained in the list; otherwise, the insertion index.
   * 
   * @throws NullPointerException
   *   when {@code list} is {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type extends Comparable<Type>> int binarySearchEnd(final List<Type> list, final Type key) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    return Lists.binarySearch(list, key, Comparator.naturalOrder(), true);
  }

  /**
   * Searches the specified, ${code comparator} defined, ordered {@code list} for the last (end exclusive) specified {@code key} using the binary search
   * algorithm. If {@code list} is not ordered, the result is undefined. This method differs from {@link Collections#binarySearch(List, Object)}, because it
   * search the last element that match keys instead of any occurrence like Java SDK.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * @param comparator
   *   the comparator by which the list is ordered.
   * 
   * @return the index after the last found search key (end exclusive), if it is contained in the list; otherwise, the insertion index.
   * 
   * @throws NullPointerException
   *   when {@code list} or {@code comparator} are {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type> int binarySearchEnd(final List<Type> list, final Type key, final Comparator<Type> comparator) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    Objects.requireNonNull(comparator, "comparator must not be null.");
    return Lists.binarySearch(list, key, comparator, true);
  }

  /**
   * Searches the specified naturally ordered {@code list} for the range (start inclusive, end exclusive) specified {@code key} using the binary search
   * algorithm. If {@code list} is not ordered, the result is undefined.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * 
   * @return the {@link BinarySearchInterval} containing, if the {@link BinarySearchInterval#found()} is {@code true}, the start index (inclusive) and the end
   *   index (exclusive) or elements containing {@code key}, if {@link BinarySearchInterval#found()} is {@code false}, both start and end indexes pointing to
   *   the insertion point.
   * 
   * @throws NullPointerException
   *   when {@code list} is {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type extends Comparable<Type>> BinarySearchInterval binarySearchInterval(final List<Type> list, final Type key) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    var start = Lists.binarySearchStart(list, key);
    var end = Lists.binarySearchEnd(list, key);
    return new BinarySearchInterval(start, end);
  }

  /**
   * Searches the specified, ${code comparator} defined, ordered {@code list} for the range (start inclusive, end exclusive) specified {@code key} using the
   * binary search algorithm. If {@code list} is not ordered, the result is undefined.
   *
   * @param <Type>
   *   the type of {@code list} elements and key (automatically determined).
   * @param list
   *   the list to be searched.
   * @param key
   *   the key to be searched for.
   * @param comparator
   *   the comparator by which the list is ordered.
   * 
   * @return the {@link BinarySearchInterval} containing, if the {@link BinarySearchInterval#found()} is {@code true}, the start index (inclusive) and the end
   *   index (exclusive) or elements containing {@code key}, if {@link BinarySearchInterval#found()} is {@code false}, both start and end indexes pointing to
   *   the insertion point.
   * 
   * @throws NullPointerException
   *   when {@code list} or {@code comparator} are {@code null}.
   */
  @SuppressWarnings("java:S1192") // SonarLint: String literals should not be duplicated. (For local contexts they should).
  public static <Type> BinarySearchInterval binarySearchInterval(
    final List<Type> list,
    final Type key,
    final Comparator<Type> comparator
  ) throws NullPointerException {
    Objects.requireNonNull(list, "list must not be null.");
    Objects.requireNonNull(comparator, "comparator must not be null.");
    var start = Lists.binarySearchStart(list, key, comparator);
    var end = Lists.binarySearchEnd(list, key, comparator);
    return new BinarySearchInterval(start, end);
  }

  private static <Type> int binarySearch(final List<Type> list, final Type key, final Comparator<Type> comparator, final boolean searchEnd) {
    var low = 0;
    var high = list.size();
    while (low < high) {
      var mid = (low + high) >> 1;
      var midValue = list.get(mid);
      if (searchEnd ? (comparator.compare(key, midValue) < 0) : (comparator.compare(key, midValue) <= 0)) {
        high = (low + high) >> 1;
        continue;
      }
      low = ((low + high) >> 1) + 1;
    }
    return low;
  }

  /**
   * A <a href="https://en.wikipedia.org/wiki/Value_object">Value object</a> that represents the result of a binary search of a ordered list which can contains
   * more than one repeated value. As the list is ordered, the result is a index interval containing all repeated values.
   *
   * @param start
   *   the start index (inclusive), or the insertion point. If start is lesser than 0, it will be normalized to 0;
   * @param end
   *   the end index (exclusive), or the insertion point. If end is lesser than 0, it will be normalized to 0;
   * 
   * @throw IllegalArgumentException when start is negative, or end is lesser than start.
   * 
   * @author Tiago van den Berg
   * 
   * @since 1.0
   */
  public static record BinarySearchInterval(int start, int end) {

    @SuppressWarnings("javadoc")
    public BinarySearchInterval {
      if (start < 0) {
        throw new IllegalArgumentException("start must not be negative.");
      }
      if (end < start) {
        throw new IllegalArgumentException("end must not be lesser than start.");
      }
    }

    /**
     * Return key was found in an interval.
     *
     * @return {@code true} if the interval contains keys, {@code false} if it contains the insertion point.
     */
    public boolean found() {
      return this.start < this.end;
    }

  }

}
