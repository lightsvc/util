// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

/**
 * Utility methods to ease the implementation of {@link Comparable#compareTo(Object)} methods. Since Java 8 {{@link Comparator} received numerous methods to
 * help implement comparations, but this utility can implement shorter implementations.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class CompareTo {

  // Forbids the instantiation.
  private CompareTo() {
    throw new UnsupportedOperationException("lightsvc.util.CompareTo cannot be instantiated.");
  }

  /**
   * Compare two objects returning a negative integer, zero, or a positive integer as {@code thisObject} is less than, equal to, or greater than
   * {@code otherObject} based on an collection of defined attributes. This method greatly simplify the implementation of the Java {@link Comparable} interface
   * ({@link Comparable#compareTo(Object)}). Use it as return body of {@code compareTo(Object)} method in a class that implements {@link Comparable}.
   * 
   * @param <T>
   *   The type of {@code thisObject} and {@code otherObject} (automatically determined).
   * @param thisObject
   *   The object reference of the self object. Use {@code this} in this parameter.
   * @param otherObject
   *   The object reference of the other object to be tested. Use the parameter argument of {@link Comparable#compareTo(Object)} method in this parameter.
   * @param attributesExtractor
   *   A {@link Function} that receives the object as parameter and should return the comparable objects attributes as an array.
   * 
   * @return a negative integer, zero, or a positive integer as {@code thisObject} is less than, equal to, or greater than {@code otherObject}.
   * 
   * @throws NullPointerException
   *   when {@code thisObject} or {@code attributesExtractor} are @{@code null}.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   public class QueryParameter extends NameValue {
   *
   *     public QueryParameter(final String name, final String value) {
   *       super(name, value);
   *     }
   *   
   *     &#64;Override
   *     public boolean compareTo(final Object other) {
   *       return <strong>CompareTo.of(this, other, o->CompareTo.attributes(o.name, o.value));</strong>
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  public static <T> int of(T thisObject, T otherObject, Function<T, Comparable<?>[]> attributesExtractor) {
    Objects.requireNonNull(thisObject, "thisObject must not be null.");
    Objects.requireNonNull(attributesExtractor, "attributesExtractor must not be null.");
    if (otherObject == null) {
      return 1;
    }
    var obj1Attributes = attributesExtractor.apply(thisObject);
    var obj2Attributes = attributesExtractor.apply(otherObject);
    int attributesLength = obj1Attributes.length;
    for (var attributeIndex = 0; attributeIndex < attributesLength; attributeIndex++) {
      var obj1Attribute = obj1Attributes[attributeIndex];
      var obj2Attribute = obj2Attributes[attributeIndex];
      if (obj1Attribute == null || obj2Attribute == null) {
        if (obj1Attribute == null && obj2Attribute == null) {
          continue;
        }
        if (obj1Attribute != null) {
          return 1;
        }
        return -1;
      }
      @SuppressWarnings({
        "unchecked", "rawtypes"
      })
      int result = ((Comparable) obj1Attribute).compareTo(obj2Attribute);
      if (result != 0) {
        return result;
      }
    }
    return 0;
  }

  /**
   * Utility semantic sugar method used to extract attributes compared in @{link CompareTo@of} {@code attributesExtractor} parameter.
   *
   * @param attributes
   *   extracted attributes to compare.
   * 
   * @return an array of comparable objects to compare.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   public class QueryParameter extends NameValue {
   *
   *     public QueryParameter(final String name, final String value) {
   *       super(name, value);
   *     }
   *   
   *     &#64;Override
   *     public boolean compareTo(final Object other) {
   *       return CompareTo.of(this, other, <strong>o->CompareTo.attributes(o.name, o.value)</strong>);
   *     }
   *   }
   *   </code>
   *   </pre>
   */
  @SuppressWarnings("java:S1452")
  public static Comparable<?>[] attributes(final Comparable<?>... attributes) {
    return attributes;
  }

}
