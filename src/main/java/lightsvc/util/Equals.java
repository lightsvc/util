// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

/**
 * Utility methods to ease the implementation of {@link Object#equals(Object)} methods.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class Equals {

  // Forbids the instantiation.
  private Equals() {
    throw new UnsupportedOperationException("usvc.util.Equals cannot be instantiated.");
  }

  /**
   * Returns if an object is equals to another based on an collection of defined attributes. This method greatly simplify the implementation of the Java Object
   * equality interface ({@link Object#equals(Object)}). Use it as return body of {@code equals(Object)} method in a subclassed type that need to test true for
   * superclasses values or if all subclassed types must test true, based on a superclass set of attributes.
   *
   * @param <ObjectType>
   *   The type of {@code thisObject} and {@code attributesExtractor} argument (determined by {@code type}).
   * @param type
   *   The type of {@code thisObject} and {@code attributesExtractor} argument.
   * @param thisObject
   *   The object reference of the self object. Use {@code this} in this parameter.
   * @param otherObject
   *   The object reference of the other object to be tested. Use the parameter argument of {@link Object#equals(Object)} method in this parameter.
   * @param attributesExtractor
   *   A {@link Function} that receives the object as parameter and should return the objects attributes as an array.
   * 
   * @return {@code true} if {@code thisObject} and {@code otherObject} references the same object or are instances of {@code type} class and attributes of both
   *   objects returned by {@code attributesExtractor} are tested equals, {@code false} otherwise.
   * 
   * @throws NullPointerException
   *   when {@code type}, {@code thisObject} or {@code attributesExtractor} are @{@code null}.
   * 
   * @example
   * 
   *   <pre>
   *   <code>
   *   public class QueryParameter extends NameValue {
   *
   *     public QueryParameter(final String name, final String value) {
   *       super(name, value);
   *     }
   *   <strong>
   *     &#64;Override
   *     public boolean equals(final Object other) {
   *       return Equals.of(NameValue.class, this, other, o->Equals.attributes(o.name, o.value));
   *     }
   *   </strong>
   *   }
   *   </code>
   *   </pre>
   */
  public static <ObjectType> boolean of(
    final Class<ObjectType> type,
    final ObjectType thisObject,
    final Object otherObject,
    final Function<ObjectType, Object[]> attributesExtractor
  ) throws NullPointerException {
    Objects.requireNonNull(type, "type must not be null.");
    Objects.requireNonNull(thisObject, "thisObject must not be null.");
    Objects.requireNonNull(attributesExtractor, "attributesExtractor must not be null.");
    if (thisObject == otherObject) {
      return true;
    }
    if ((otherObject == null) || !type.isAssignableFrom(otherObject.getClass())) {
      return false;
    }
    @SuppressWarnings("unchecked")
    final var castObject2 = (ObjectType) otherObject;
    return Arrays.equals(attributesExtractor.apply(thisObject), attributesExtractor.apply(castObject2));
  }

  /**
   * Utility semantic sugar method used to extract attributes compared in @{link Equals@of} {@code attributesExtractor} parameter.
   *
   * @param attributes
   *   extracted attributes to compare.
   * 
   * @return an array of objects to compare.
   * 
   * @example
   * 
   *   <pre>
   * <code>Equals.of(NameValue.class, this, other, <strong>o->Equals.attributes(o.name, o.value)</strong>);</code>
   *   </pre>
   */
  public static Object[] attributes(final Object... attributes) {
    return attributes;
  }

}
