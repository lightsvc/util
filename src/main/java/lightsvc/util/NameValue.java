// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.Comparator;
import java.util.Objects;

/**
 * A <a href="https://en.wikipedia.org/wiki/Value_object">Value object</a> simple class encapsulating a name/value pair.
 *
 * @param name
 *   the name
 * @param value
 *   the value
 * 
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public record NameValue(String name, String value) implements Comparable<NameValue> {

  /**
   * Syntactic sugar for {@link NameValue#NameValue(String, String)}.
   *
   * @param name
   *   the name
   * @param value
   *   the value
   * 
   * @return a newly create {@link NameValue}
   */
  public static NameValue of(final String name, final String value) {
    return new NameValue(name, value);
  }

  /**
   * Syntactic sugar for create inline arrays of {@link NameValue}s.
   *
   * @param nameValues
   *   the name
   * 
   * @return an array of {@link NameValue}
   */
  public static NameValue[] nameValues(final NameValue... nameValues) {
    return nameValues;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @see Object#equals(Object)
   * 
   * @return {@code true} if {@code this} and {@code other}, {@link #name()} and {@link #value()} are equal, {@code false} otherwise.
   */
  @Override
  public boolean equals(final Object other) {
    return Equals.of(NameValue.class, this, other, o->Equals.attributes(o.name, o.value));
  }

  /**
   * @see Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.value);
  }

  /**
   * Compares this object with the specified object for order, first using {@link #name()} and then {@link #value()}.
   *
   * @see Comparable#compareTo(Object)
   * 
   * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
   */
  @Override
  public int compareTo(final NameValue other) {
    return Comparator.comparing(NameValue::name, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(Comparator.comparing(NameValue::value, Comparator.nullsFirst(Comparator.naturalOrder())))
      .compare(this, other);
  }

}
