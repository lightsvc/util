// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Common utility methods, not present in Java SDK, or perform better, to handle {@link String}s.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class Strings {

  // Forbids the instantiation.
  private Strings() {
    throw new UnsupportedOperationException("lightsvc.util.Strings cannot be instantiated.");
  }

  /**
   * Verifies if a string contains ISO control codes, non-printable characters, and not desirable characters in forms fields characters as line breaks,
   * tabulations or null characters.
   *
   * @param value
   *   The {@link String} to verify.
   * 
   * @return {@code true} if {@code value} contains control codes or is {@code null} (to avoid unsafe conditions) or {@code false} if it not contains control
   *   codes.
   */
  public static boolean containsIsoControlCodes(final String value) {
    if (value == null) {
      return true;
    }
    final var charStream = value.chars();
    return charStream.anyMatch(Character::isISOControl);
  }

  /**
   * Verifies if a string is not null and not empty.
   *
   * @param value
   *   The {@link String} to verify.
   * 
   * @return {@code true} if {@code value} is not null and not an empty {@link String}, {@code false} otherwise.
   */
  public static boolean isNotEmpty(final String value) {
    return (value != null) && !value.isEmpty();
  }

  /**
   * Verifies if a string is null or empty.
   *
   * @param value
   *   The {@link String} to verify.
   * 
   * @return {@code true} if {@code value} is null or an empty {@link String}, {@code false} otherwise.
   */
  public static boolean isNullOrEmpty(final String value) {
    return (value == null) || value.isEmpty();
  }

  /**
   * Return a empty {@link String} if value is {@code null}. This can be useful as a way to avoid {@link NullPointerException} on user inputs.
   *
   * @param value
   *   A possibly {@code null} {@link String}.
   * 
   * @return {@code value} itself when not {@code null} or an empty string if {@code null}.
   */
  public static String nullSafe(final String value) {
    return DefaultValue.of(value, "");
  }

  /**
   * Return a {@link String} removing a specific character.
   *
   * @param value
   *   The {@link String} to have {@code charToRemove} glyphs removed.
   * @param charToRemove
   *   The character glyph that will be filtered out.
   * 
   * @return The {@link String} with characters removed or {@code null} if {@code value} is {@code null}.
   */
  public static String removeChars(final String value, final char charToRemove) {
    if (value == null) {
      return null;
    }
    // Create a buffer to copy all characters excluding charToRemove.
    var sb = new StringBuilder();
    // From first char...
    var startFrom = 0;
    while (true) {
      // Try to find the next charToRemove in value.
      var indexOf = value.indexOf(charToRemove, startFrom);
      // If not found, copy all the left chars and exit with the result.
      if (indexOf < 0) {
        sb.append(value.substring(startFrom));
        return sb.toString();
      }
      // If the next char is a charToRemove ignore the copy now, it will be removed in batch later.
      if (indexOf == startFrom) {
        startFrom = indexOf + 1;
        continue;
      }
      // If there is chars to maintain, copy theses chars to the buffer.
      sb.append(value.substring(startFrom, indexOf));
      startFrom = indexOf + 1;
    }
  }

  /**
   * Return a {@link String} replacing markup tags with values from a custom user function. <br />
   * The markup tags must have a start and a end sequence of chars that delimits it. For example: {@code "$(" } and {@code ")"}. <br/>
   * *Note that this function not support html tags. For this purpose it is preferable the use of a DOM parser.
   *
   * @param value
   *   The {@link String} to have markups replaced.
   * @param markupTagStart
   *   A String that marks a tag start.
   * @param markupTagEnd
   *   A String that marks a tag end.
   * @param replacer
   *   A custom {@link Function} that receive as argument the {@link String} content between, not including {@code markupTagStart} and {@code markupTagEnd}, and
   *   returns a NON-NULL {@link String} if the entire tag should be replaced or {@code null}, if tag should NOT be replaced.
   * 
   * @return The string with markup tags replaced, or ${code value} unmodified value if any of {@code value}, {@code markupTagStart} , {@code markupTagEnd} ,
   *   {@code replacer} are {@code null}.
   */
  public static String replaceMarkup(final String value, final String markupTagStart, final String markupTagEnd, final UnaryOperator<String> replacer) {
    if (Strings.isNullOrEmpty(value) || Strings.isNullOrEmpty(markupTagStart) || Strings.isNullOrEmpty(markupTagEnd) || (replacer == null)) {
      return value;
    }
    var result = value;
    // Starting the searchPosition from beginning of value...
    var searchPosition = 0;
    while (true) {
      // Search for the markupTagStart in sentence.
      final var start = result.indexOf(markupTagStart, searchPosition);
      // If it was not found or none was left, return the result.
      if (start < 0) {
        return result;
      }
      // If the markupTagStart is found, search for the markupTagEnd.
      final var end = result.indexOf(markupTagEnd, start + markupTagStart.length());
      // If it was not found, consider it a invalid markup and return the result the way it is.
      if (end < 0) {
        return result;
      }
      // Isolate the tagName and call the replacer to replace its value.
      final var tagName = result.substring(start + markupTagStart.length(), end);
      final var replacedTagName = replacer.apply(tagName);
      // If the tagName was not replaced (returned null) continue searching for new markups.
      if (replacedTagName == null) {
        searchPosition = end + markupTagEnd.length();
        continue;
      }
      // If the tagName was replaced, replace it on the result, adjust the searchPosition to the end of replacedTagName and continue searching for new markups.
      result = result.substring(0, start) + replacedTagName + result.substring(end + markupTagEnd.length());
      searchPosition = start + replacedTagName.length();
    }
  }

  /**
   * Return a newly create array with segments of a {@link String} split by a specific character delimiter.
   *
   * @param value
   *   The {@link String} to split in segments.
   * @param delimiter
   *   The character glyph that will be use to separate {@code value} in segments.
   * 
   * @return An array of {@link String} with elements as segments of {@code value}.
   */
  public static String[] split(final String value, final char delimiter) {
    
    if (value == null) {
      return new String[] {};
    }
    final var HALF = 2;
    final var INITIAL_AND_FINAL_TOKENS = 2;
    var maxPossibleArray = new String[(value.length() / HALF) + INITIAL_AND_FINAL_TOKENS];
    var wordCount = 0;
    var startFrom = 0;
    var delimiterIndex = value.indexOf(delimiter, 0); // first substring
    while (delimiterIndex >= 0) {
      maxPossibleArray[wordCount] = value.substring(startFrom, delimiterIndex);
      wordCount++;
      startFrom = delimiterIndex + 1;
      delimiterIndex = value.indexOf(delimiter, startFrom); // rest of substrings
    }
    maxPossibleArray[wordCount] = value.substring(startFrom); // last substring
    wordCount++;
    var result = new String[wordCount];
    System.arraycopy(maxPossibleArray, 0, result, 0, wordCount);
    return result;
  }

  /**
   * Return a {@link String} removing a leading and trailing specific character.
   *
   * @param value
   *   The {@link String} to have leading and trailing {@code charToTrim} glyphs removed.
   * @param charToTrim
   *   The character glyph that will be removed.
   * 
   * @return The {@link String} with leading and trailing characters removed.
   */
  public static String trim(final String value, final char charToTrim) {
    if (value == null) {
      return null;
    }
    var begin = 0;
    var end = value.length() - 1;
    while ((begin <= end) && (value.charAt(begin) == charToTrim)) {
      begin++;
    }
    while ((end > begin) && (value.charAt(end) == charToTrim)) {
      end--;
    }
    if (begin > end) {
      return "";
    }
    return value.substring(begin, end + 1);
  }

}
