// This file is part of lightsvc Java Microservices Components (lightsvc).
//
// lightsvc Java Microservices Components (lightsvc) is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// lightsvc Java Microservices Components (lightsvc) is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with lightsvc Java Microservices Components (lightsvc). If not, see <http://www.gnu.org/licenses/>.
package lightsvc.util;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Common utility methods, not present in Java SDK, to handle maps. <br/>
 * Unlike the Java standard API counterparts, these utilities methods allow the use of Different map implementations and the use of null keys and values.
 *
 * @since 1.0
 * 
 * @author Tiago van den Berg
 */
public final class Maps {

  // Forbids the instantiation.
  private Maps() {
    throw new UnsupportedOperationException("lightsvc.util.Maps cannot be instantiated.");
  }

  /**
   * Utility semantic sugar method used to create an <code>int</code> key {@link Map}.{@link Entry} to be used in {@link #of(Map, Entry...)}.
   *
   * @param <Value>
   *   Generic type of {@link Map}.{@link Entry} value (automatically defined).
   * @param key
   *   {@link Map}.{@link Entry} <code>int</code> key.
   * @param value
   *   {@link Map}.{@link Entry} value.
   * 
   * @return a newly created {@link Map}.{@link Entry}.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), entry(1, "value1"), entry(2, "value2"));
   *   }
   *   </pre>
   */
  public static <Value> Map.Entry<Integer, Value> entry(final int key, final Value value) {
    return new AbstractMap.SimpleEntry<>(Integer.valueOf(key), value);
  }

  /**
   * Utility semantic sugar method used to create a {@link Map}.{@link Entry} to be used in {@link #of(Map, Entry...)}. <br />
   * This method differs from {@link Map#entry(Object, Object)}, that it allows {@code null} keys and values.
   *
   * @param <Key>
   *   Generic type of {@link Map}.{@link Entry} key (automatically defined).
   * @param <Value>
   *   Generic type of {@link Map}.{@link Entry} value (automatically defined).
   * @param key
   *   {@link Map}.{@link Entry} key.
   * @param value
   *   {@link Map}.{@link Entry} value.
   * 
   * @return a newly created {@link Map}.{@link Entry}.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), entry("key1", "value1"), entry("key2", "value2"));
   *   }
   *   </pre>
   */
  public static <Key, Value> Map.Entry<Key, Value> entry(final Key key, final Value value) {
    return new AbstractMap.SimpleEntry<>(key, value);
  }

  /**
   * Utility semantic sugar method used to create a keys array to be used in {@link #ofKeysAndValues(Map, Object[], Object[])}.
   *
   * @param <Key>
   *   Generic type of {@link Map}.{@link Entry} key (automatically defined).
   * @param keys
   *   {@link Map}.{@link Entry} key.
   * 
   * @return The {@code keys} array.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), entry("key1", "value1"), entry("key2", "value2"));
   *   }
   *   </pre>
   */
  @SuppressWarnings("unchecked")
  public static <Key> Key[] keys(final Key... keys) {
    return keys;
  }

  /**
   * Merge a collection of maps entries to a existing Map (The common case is to create one inline).
   *
   * @param <MapValue>
   *   Generic type of {@code map} (automatically defined).
   * @param <Key>
   *   Generic type of {@code map} key (automatically defined).
   * @param <Value>
   *   Generic type of {@code map} value (automatically defined).
   * @param map
   *   An existing {@link Map} or {@code null} (The common case is to create one inline).
   * @param mapsToAdd
   *   a collection of maps to merge (putAll). {@code null} maps are ignored. The later the map the more priority their values have and they overwrite prior
   *   maps values.
   * 
   * @return {@code map} with entries based on keys and values merged, or null if {@code map} is {@code null}.
   */
  @SafeVarargs
  public static <MapValue extends Map<Key, Value>, Key, Value> Map<Key, Value> merge(final MapValue map, final MapValue... mapsToAdd) {
    if (map == null) {
      return map;
    }
    for (var mapToAdd : mapsToAdd) {
      if (mapToAdd == null) {
        continue;
      }
      map.putAll(mapToAdd);
    }
    return map;
  }

  /**
   * Add an entry (key and value) to an existing Map (The common case is to create one inline) in an one-liner style. <br />
   * Support read-write and specific map types that {@link Map#of()} do not.
   *
   * @param <MapValue>
   *   Generic type of {@code map} (automatically defined).
   * @param <Key>
   *   Generic type of {@code map} key (automatically defined).
   * @param <Value>
   *   Generic type of {@code map} value (automatically defined).
   * @param map
   *   An existing {@link Map} or {@code null} (The common case is to create one inline).
   * @param key
   *   {@link Map}.{@link Entry} key to add.
   * @param value
   *   {@link Map}.{@link Entry} value to add.
   * 
   * @return {@code map} with {@code entries} added, or null if {@code map} is {@code null}.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), "key1", "value1");
   *   }
   *   </pre>
   */
  public static <MapValue extends Map<Key, Value>, Key, Value> MapValue of(final MapValue map, final Key key, final Value value) {
    if (map == null) {
      return map;
    }
    map.put(key, value);
    return map;
  }

  /**
   * Add entries to an existing Map (The common case is to create one inline) in an one-liner style. <br />
   * Support read-write and specific map types that {@link Map#of()} do not.
   *
   * @param <MapValue>
   *   Generic type of {@code map} (automatically defined).
   * @param <Key>
   *   Generic type of {@code map} key (automatically defined).
   * @param <Value>
   *   Generic type of {@code map} value (automatically defined).
   * @param map
   *   An existing {@link Map} or {@code null} (The common case is to create one inline).
   * @param entries
   *   Entries to add in {@code map}. {@code null} entries are ignored.
   * 
   * @return {@code map} with {@code entries} added, or null if {@code map} is {@code null}.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), entry("key1", "value1"), entry("key2", "value2"));
   *   }
   *   </pre>
   */
  @SafeVarargs
  public static <MapValue extends Map<Key, Value>, Key, Value> MapValue of(final MapValue map, final Map.Entry<Key, Value>... entries) {
    if ((map == null) || (entries == null)) {
      return map;
    }
    for (var entry : entries) {
      if (entry == null) {
        continue;
      }
      map.put(entry.getKey(), entry.getValue());
    }
    return map;
  }

  /**
   * Add a collection of entries to a existing Map (The common case is to create one inline) using pairs formed by items of arrays of keys and values with same
   * indexes. <br />
   *
   * @param <MapValue>
   *   Generic type of {@code map} (automatically defined).
   * @param <Key>
   *   Generic type of {@code map} key (automatically defined).
   * @param <Value>
   *   Generic type of {@code map} value (automatically defined).
   * @param map
   *   An existing {@link Map} or {@code null} (The common case is to create one inline).
   * @param keys
   *   {@link Map}.{@link Entry} keys to add. <br />
   *   If {@code keys}.length is smaller than {@code values}.length, only add {@code keys}.length entries to {@code map}.
   * @param values
   *   {@link Map}.{@link Entry} keys to add. <br />
   *   If {@code values}.length is smaller than {@code keys}.length, only add {@code values}.length entries to {@code map}.
   * 
   * @return {@code map} with entries based on keys and values added, or null if {@code map} is {@code null}.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.ofKeysAndValues(new TreeMap<>(), keys("key1", "key2"), values("value1", "value2"));
   *   }
   *   </pre>
   */
  public static <MapValue extends Map<Key, Value>, Key, Value> Map<Key, Value> ofKeysAndValues(final MapValue map, final Key[] keys, final Value[] values) {
    if ((map == null) || (keys == null) || (values == null)) {
      return map;
    }
    final var length = Math.min(keys.length, values.length);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      map.put(key, values[i]);
    }
    return map;
  }

  /**
   * Utility semantic sugar method used to create a values array to be used in {@link #ofKeysAndValues(Map, Object[], Object[])}.
   *
   * @param <Value>
   *   Generic type of {@link Map}.{@link Entry} value (automatically defined).
   * @param values
   *   {@link Map}.{@link Entry} key.
   * 
   * @return The {@code values} array.
   * 
   * @example
   * 
   *   <pre>
   *   {@code
   *   Maps.of(new TreeMap<>(), entry("key1", "value1"), entry("key2", "value2"));
   *   }
   *   </pre>
   */
  @SuppressWarnings("unchecked")
  public static <Value> Value[] values(final Value... values) {
    return values;
  }

}
